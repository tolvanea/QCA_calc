#!/usr/bin/python3


"""
This script reads one line from conf file and returns it

usage (running directly)
    read_conf.py -f CONF_FILE -r KEY_VALUE_TO

"""

from sys import argv
from sys import exit
import os.path


def read_config_line_pair(file_name, value_to_read):
    """
    This function reads given config file, and returns the value for key. Key and value are
    separated with space(s) in file.
    :param file_name:       path to file (string)
    :param value_to_read:   key (string)


    """
    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            value_pair = line.split()
            if (len(value_pair) != 2):    # skips newlines and unknown '='-formatting
                continue
            if (value_pair[0]==value_to_read):
                return value_pair[1]

        if (value_to_read!=""):
            print("    Nothing found with", value_to_read)
            print("    Continuing")


def read_config_line_values(file_name, value_to_read):
    """
    This function reads given config file, and returns the value for key. Key and value are
    separated with space(s) in file.
    
    Todo: Question? How this differs from one above?
        - this is more strict on format of config file or something
    
    :param file_name:       path to file (string)
    :param value_to_read:   key (string)


    """
    with open(file_name, "r") as conf_file:

        file_lines = conf_file.readlines()

        for line_indx, line in enumerate(file_lines):
            parts = line.split()
            if (parts[0]==value_to_read):
                if (len(parts) < 2):    # skips newlines and unknown '='-formatting
                    raise Exception("Key must be separated with spaces")
                return parts[1:]


        raise Exception("Nothing found with", value_to_read)


def main_function_if_executed_directly(argv):
    """
    This is main function, and is executed if this "module" is called directly.
    Therefore this can be called from bash.
    """

    # General error checking

    # Get file name
    file_name = ""
    if (("-f" in argv) and (argv.index("-f")+1 < len(argv))):
        file_name = argv[argv.index("-f")+1]
    else:
        print("    No file given")
        print("    Usage: read_conf.py -f path/file.txt -r key_to_value")
        print("    Nothing is read")
        exit(2)

    if (("-r" in argv) and (argv.index("-r")+1 < len(argv))):
        value_to_read = argv[argv.index("-r")+1]
    else:
        print("    No line to read given")
        print("    Usage: read_conf.py -f path/file.txt -r key_to_value")
        print("    Nothing is read")
        exit(2)

    # check that file exist
    if (not os.path.isfile(file_name)):
        print("    No file", file_name, "exist")
        print("    Nothing is read")
        exit(2)


    # The conf reading
    val = read_config_line_pair(file_name, value_to_read)

    print("Value is", val)


if __name__ == "__main__":
    main_function_if_executed_directly(argv)

