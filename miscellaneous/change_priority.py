#!/usr/bin/python3

"""
This script lowers the priority of all matching programs
Usage
python3 -m change_priority pimc3
"""

import psutil
import os
import sys


def main(argv):
    process_name = argv[1]

    if (len(argv) >= 2):
        priority = argv[2]
    else:
        priority = "low"

    if not (priority in "high,medium,low"):
        raise Exception("Unkown priority '" + argv[2] + "'")

    changed_something = False
    for proc in psutil.process_iter():
        if proc.name() == process_name:

            nice_old = proc.nice()

            if (priority == "high"):
                proc.nice(-10)
            elif (priority in "medium"):
                proc.nice(0)
            elif (priority == "low"):
                proc.nice(10)

            nice_new = proc.nice()

            #print(proc.name(), proc.pid, " nice:", nice_old, "->", nice_new)
            changed_something = True

    if not changed_something:
        print("No process found with name '" + process_name + "'")
        #raise Exception("No process found with name '" + process_name + "'")


if __name__ == "__main__":
    main(sys.argv)
