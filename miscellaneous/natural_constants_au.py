import numpy as np
#import matplotlib.pyplot as plt

_E_h = 4.35974465054e-18    # energia
_m_e = 9.1093835611e-31     # elektronin massa
_e   = 1.602176620898e-19   # alkeisvaraus
_h_  = 1.05457172647e-34    # redusoitu plank
_k_e = 8.9875517873681e9    # electrinen vakio
_a0  = 5.291772109217e-11   #pituus
_t   = _h_/_E_h          # aika
_p   = _h_/_a0           #liikemäärä
_v   = _a0 * _E_h/_h_   # nopeus
_E   = _E_h/(_e*_a0)    # sähkökenttä
_V   = _E_h/_e           # potentiaali
_omega = _h_
_k_B = 1.38064852e-23;     # boltsmannin vakio
_T = _E_h/_k_B                 # lämpötila

alfa = 137.03599913931    # Fine-structure constant
c = 1/alfa
m_e = 1
e = 1
h_ = 1
h = 2*np.pi
k_e = 1
k_B = 1
eps0 = 1/(4*np.pi)
a0 = 1
eV = 1/27.2113860217
omega = h_          # angular momentum


# SI -> au
class conv_SI:
    
    E = 1 / _E_h
    T = 1 / _T
    def __init__(self):
        raise Exception()
    







"""
Atomic units (au or a.u.) form a system of natural units which is especially convenient for atomic physics calculations. There are two different kinds of atomic units, Hartree atomic units[1] and Rydberg atomic units, which differ in the choice of the unit of mass and charge. This article deals with Hartree atomic units, where the numerical values of the following four fundamental physical constants are all unity by definition:

    Electron mass m e;
    Elementary charge e;
    Reduced Planck's constant ℏ = h / ( 2 π ) ;
    Coulomb's constant k e = 1 / ( 4 π ϵ 0 ) .



In Hartree units, the speed of light is approximately 137 {\displaystyle 137} 137. Atomic units are often abbreviated "a.u." or "au", not to be confused with the same abbreviation used also for astronomical units, arbitrary units, and absorbance units in different contexts.


Atomic units are chosen to reflect the properties of electrons in atoms. This is particularly clear from the classical Bohr model of the hydrogen atom in its ground state. The ground state electron orbiting the hydrogen nucleus has (in the classical Bohr model):

    Orbital velocity = 1
    Orbital radius = 1
    Angular momentum = 1
    Orbital period = 2π
    Ionization energy =  1⁄2
    Electric field (due to nucleus) = 1
    Electrical attractive force (due to nucleus) = 1

"""
