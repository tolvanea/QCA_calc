# WARNING NOT TESTED (update: not implemented in project)
import sys
import os
import miscellaneous.logger as logger
log = logger.get_logger(__name__, level="DEBUG", disabled=False, colors=True)

"""
This file contains hacks to plot tikz pictures with errorbars in it (matplotlib2tikz is very buggy).
The method is that some code is insered in generated file. Ugly, but works if used correctly.
"""


plt2tikZ_is_crappy = False
if not plt2tikZ_is_crappy:
    from matplotlib2tikz import save as tikz_save
    # https://github.com/nschloe/matplotlib2tikz
else:
    def tikz_save(asdf):
        pass

def block_print(): 
    sys.stdout = open(os.devnull, 'w')


def enable_print():
    sys.stdout = sys.__stdout__


def write_lines(filename, contains, insert_this):
    """
    Inserts text 'insert_this' in file 'filename' after the line that contains 'contains'
    """
    with open(filename, 'r') as file:
        data = file.readlines()

    for idx, line in enumerate(data):
        if contains in line:
            data.insert(idx+1, insert_this)
            break

    with open(filename, 'w') as file:
        file.writelines(data)


def fix_labels_in_tikz(*labels) -> str:
    """
    Generates the required insertion for to get line labels working in tikz
    
    The whole insertion looks something like this in a whole:
    
    insertion = \
        "legend entries={{" + label1 + "},{" + label2 + "},{" + label3 + "}},"+ "\n" +\
        "legend cell align={right}," + "\n" +\
        "legend style={draw=white!80.0!black}," + "\n" +\
        "legend style={at={(0.03,1.03)},anchor=south west}" + "\n" +\
        "]  % replace the ending bracket" + "\n" +\
        "\\addlegendimage{no markers, color0}"+ "\n" +\
        "\\addlegendimage{no markers, color1}"+ "\n" +\
        "\\addlegendimage{no markers, color2}%" 
    
    """
    
    insertion = ",\n"  # text insertion in tikz file after line "y grid style=..."
    
    label_entries = "legend entries={"
    for i in range(len(labels)):
        label_entries += "{" + labels[i] + "}"
        if i != len(labels)-1:
            label_entries += ","
    label_entries += "},\n"
    insertion += label_entries
    
    insertion += "legend cell align={right},\n"
    insertion += "legend style={draw=white!80.0!black},\n"
    insertion += "legend style={at={(0.1,1.03)},anchor=south west}\n"
    insertion += "]  % replace the ending bracket\n"
    
    for i in range(len(labels)):
        insertion += "\\addlegendimage{no markers, color" + str(i) + "}"
        
        if i != len(labels)-1:
            insertion += "\n"
        else:
            insertion += "%"
    
    return insertion


def tikz_save_with_labels(pathname, labels):
    
    block_print()
    tikz_save(pathname)
    enable_print()
        
    log.debug("Adding lines to file 'gridsizes.tikz' in part 'begin{axis}['" + "\n")
        
    add_label = fix_labels_in_tikz(*labels)
    write_lines('figures/gridsizes.tikz', "y grid style", add_label)


"""
### FOR EXAMPLE USE

def plot_multiple(x_axis, list_y, xlabel="", list_label=[], linestyles=[], 
                  log_scale=False, savename=None, ylim=None):
    fig = plt.figure(xlabel+str(savename))
    if log_scale:
        fig.gca().set_xscale("log", nonposx='clip')
        
    if len(list_label) == 0:
        list_label_0 = ["" for i in range(len(list_y))]
    else:
        list_label_0 = list_label
    
    if len(linestyles) == 0:
        linestyles_0 = ["-" for i in range(len(list_y))]
    else:
        linestyles_0 = linestyles
    
    for i, y in enumerate(list_y):
        plt.plot(x_axis, y, label=list_label_0[i], linestyle=linestyles_0[i])

    
    plt.xlabel(xlabel)
    plt.ylabel("P")
    plt.ylim(ylim)
    
    if len(list_label) > 0:
        plt.legend()
    
    if savename is not None:
        block_print()
        tikz_save("figures/" + savename + '.tikz')
        enable_print()


def plot_multiple_errorbar(x_axis, 
                           list_y, 
                           list_yerr, 
                           xlabel="", 
                           list_label=[], 
                           log_scale=False, 
                           savename=None, 
                           ylim=None,
                           linestyles=[]):
    fig = plt.figure(xlabel+" "+ savename)
    if log_scale:
        fig.gca().set_xscale("log", nonposx='clip')
        
    if len(list_label) == 0:
        list_label_0 = ["" for i in range(len(list_y))]
    else:
        list_label_0 = list_label
    
    if len(linestyles) == 0:
        linestyles_0 = ["-" for i in range(len(list_y))]
    else:
        linestyles_0 = linestyles
    
    
    for i, y in enumerate(list_y):
        plt.errorbar(x_axis, y, yerr=list_yerr[i], capsize=4, label=list_label_0[i], linestyle=linestyles_0[i])

    plt.xlabel(xlabel)
    plt.ylabel("P")
    plt.ylim(ylim)
    
    if True:
        block_print()
        tikz_save("figures/" + savename + '.tikz')
        enable_print()
        
        if len(list_label) > 0:
            plt.legend()
                         
        add_label = fix_labels_in_tikz(*list_label)
        write_lines("figures/" + savename + '.tikz', "y grid style", add_label)

    else:
        if len(list_label) > 0:
            plt.legend()    
        plt.savefig("figures/" + savename + '.pgf')
"""
