import numpy as np
#import matplotlib.pyplot as plt

h=6.6260755e-34;        # plankin vakio
h_= h/(2*np.pi);        # redusoitu plankin vakio
e=1.602176620898e-19;   # alkeisvaraus
e_q=e   # alkeisvaraus
N_A=6.022e23;           # Avogadron vakio
eps0=8.85419e-12;       # tyhjiön permittiivisyys
myy0=1.257e-6;          # ???tyhjiön permiabiliteetti???
k_B=1.38064852e-23;     # boltsmannin vakio
k_q=1/(4*np.pi*eps0);   # sähkölaskujen vakio
F_a=96500;              # faradayn vakio
m_e=9.1093897e-31;      # elektronin massa
m_p=1.6726231e-27;      # protonin massa
m_u=1.6605402e-27;      # alkeismassayksikkö tjtn
m_n=1.6749286e-27;      # neutronin massa
c=299792458;            # valon nopeus
G = 6.674e-11;          # ??gravitatiovakio???
a0 = 5.291772e-11;      # bohrin säde
eV = 1.602176620898e-19 # elektronivoltti




m_alfa = 4.001506179127*m_u;   # alfahiukkasen massa
m_H1 = 1.007825*m_u;           # vetyatomin massa


# Hartee konfiguraatiolle, SI -> SI on aina sama
class conv_SI:
    E = 1
    T = 1
