#!/usr/bin/env python3
# run this with python3:
#   $ python3 plot_it.py

import result_analysis.plotting.plot_side_and_corner as plot_side_and_corner

plot_side_and_corner.main()
