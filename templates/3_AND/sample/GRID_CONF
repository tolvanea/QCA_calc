# This file is config file for "qca_tiles" path generator. 
# This file must be placed in same folder in which the program is run.
# Following files and directories are generated: paths/, PARTICLES, QCA, QD, 

# Comments can be written like this
# Indentations, or excess spaces/tabs does not matter
# Every different part of this config file is noted by DEFINE_, and the order of
# parts must not be changed.


DEFINE_GRID
    # following values will be referred as 'dimensions'
    GRID_SIZE_a 2.0     # cell side length
    GRID_SIZE_d 0.5     # excess distance between cells
    # GRID_SIZE_l 4.5   # distance between center points of cell (commented out)
                        # --> either d or l can be used, l = 2*a + d



DEFINE_PARTICLES
    # These particles will be added in order to PARTICLES-file (only if used in DEFINE_CELLS)

  #|name|charge|mass   |something|
    el  -1.0    1.0     5   0   # electron (used in QCA_CELL)
    ps  0.5     1.0     0   0   # positive quantum dot (used in QCA_CELL)
    p1  0.5     1.0     0   0   # positive quantum dot (used in DRIVER 1)
    m1  -0.5    1.0     0   0   # negative quantum dot (used in DRIVER 1)
    p2  0.5     1.0     0   0   # positive quantum dot (used in DRIVER 2)
    m2  -0.5    1.0     0   0   # negative quantum dot (used in DRIVER 2)
    p3  0.5     1.0     0   0   # positive quantum dot (used in DRIVER 3)
    m3  -0.5    1.0     0   0   # negative quantum dot (used in DRIVER 3)



DEFINE_CELLS
    # Format: "cell_type:x", where:
    #       cell_type = {QCA_CELL, DRIVER, QCA_TRACKED, CUSTOM}
    #                   # {QCA_CELL, DRIVER} are pre-defined
    #                   # {QCA_TRACKED, CUSTOM} must be constructed by hand (see the end of this file)
    #       x is the unique letter used in DEFINE_TILING (free to chooce)

    QCA_CELL:C      # Uses dimensions from grid, and is tracked qca cell
                    # uses particles "ps" and "el"
    
    # NORMAL DRIVER NOT CURRENTLY USED
    # DRIVER:D        # Uses dimensions from grid, is four fixed charges {ps mn ps mn}
    #                 # uses "ps" and "mn" particles

    
    # Driver that uses different quantumdots (p1 and m1 instead of ps and mn)
    CUSTOM:X BEGIN
        p1 -1.0 1.0 0.0
        m1 1.0  1.0 0.0
        p1 1.0 -1.0 0.0
        m1 -1.0 -1.0 0.0
    CUSTOM:X END
    
    CUSTOM:Y BEGIN
        p2 -1.0 1.0 0.0
        m2 1.0  1.0 0.0
        p2 1.0 -1.0 0.0
        m2 -1.0 -1.0 0.0
    CUSTOM:Y END
    
    CUSTOM:Z BEGIN
        p3 -1.0 1.0 0.0
        m3 1.0  1.0 0.0
        p3 1.0 -1.0 0.0
        m3 -1.0 -1.0 0.0
    CUSTOM:Z END
    
    


DEFINE_TILING
# In this part cells defined above can be placed on grid-pattern.
# Cells are read column by column from left to right, (and column is read from up to down).
# Every line must start and end with '|'-symbol, and must be the same length.

| X|
|ZC|
| Y|



# everything after this is not read, and can be used as copypasta comment-section
END_OF_CONFIG













So this section of file will not be read by program

    ##############
     Code samples:
    ##############

samples for DEFINE_CELLS-part

    # This is and example of custom qca-tracked cell.
    # If it used, QCA_CELL must not be used at the same time (only one types of moves are supported)
    QCA_TRACKED:Q BEGIN
        ps -1.0 1.0 1.0     # coordinates are from center-point of the cell
        ps 1.0 1.0 -1.0
        ps 1.0 -1.0 1.0
        ps -1.0 -1.0 -1.0
        el 1.0 1.0 -1.0
        el -1.0 -1.0 -1.0
    QCA_TRACKED:Q END

    # this is and example of custom cell
    CUSTOM:A BEGIN
        ps -1.0 1.0 1.0
        ps 1.0 1.0 -1.0
        xx 1.0 -1.0 1.0         # assuming xx is defined in DEFINE_PARTICLES
        mn -1.0 -1.0 -1.0
        el 1.0 1.0 -1.0
    CUSTOM:A END




samples in DEFINE_TILING
    NOT-gates
        |   CC|
        |DCC  |

        |  CCC  |
        |DCC  CC|
        |  CCC  |

        |   C   |
        |DCC C  |
        |   C CC|
        |    C  |

    AND or OR gate (D is 1 or 0)
        | C |
        |DCC|
        | C |
    
    using custom tiles
        |AQQQ|

