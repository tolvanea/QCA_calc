import os
import numpy as np
import matplotlib.pyplot as plt
import subprocess
from sys import argv

import calculation_initialization.run_set.run_set as run_set
import calculation_initialization.qca_tiles.main as qca_tiles
import miscellaneous.conf_modification as conf_modification






help_text = """
This is portable file that is used to create full pimc3-calculation set of variating parameter value. It s used to initialize the set of calculation for 'run_set'.

Args:
    --help          prints this text
    --continue      continue already existing set
    --no-run        generates file-structure, but does not run pimc3

Usage:
    1. modify this file as you want to change the calculation

    2. make sure it is in 'sample'-directory with:
            GRID_CONF
            CONF

    3. python3 variate_and_run   #run this file


"""

# first variating value , temperature
dirname1 = "var_nothing"
range1 = [0]
latex_symbol1 = ["X", "x"]         # symbol and unit, e.g. "\\lambda" (backshlashes not tested)


# second variating value ()
dirname2 = "var_q"
range2 = np.linspace(-0.5, 0.5, 11)  # charge of driver quantum dots
latex_symbol2 = ["q", "e"]


# name of executable, must be found in $PATH
binary = "pimc3"


"""
Following function 'variate_value' is run every time iteratively to modify 
slightly different configuration for calulation. Right after this function 
pimc3-simulation software is run.

# directory tree:

for num1 in directories_1
    for num2 in directories_2
        variate_value()
"""
def variate_value(num1: float, num2: float) -> None:
    """
    Modify driver quantum dot charge 
    
    line in GRID_CONF:
        pd  0.5     1.0     0   0 
    to:
        pd  0.x     1.0     0   0 
    """
    
    pd = num2
    mn = -num2
    
    # quantum dot charge
    conf_modification.modify_line("GRID_CONF", key="pd", word_idx=0, replacement=str(pd), range=(21, 28))
    conf_modification.modify_line("GRID_CONF", key="mn", word_idx=0, replacement=str(mn), range=(21, 28))
    
    # store only one good density map. (They do take a lot of disk space)
    if (num1 == range1[-1] and  num2 == range2[-1]):
        conf_modification.old_bash_style_config_modifier(
                ["<nullarg>", "-f", "CONF", "--DensitySize=256"])
    
    qca_tiles.main(["Null argument"])


if __name__ == "__main__":
    
    if len(argv) <= 1:
        run_set.new_calc(dirname1, dirname2, range1, range2, variate_value, binary, latex_symbol1, latex_symbol2)
    else:
        if (argv[1] == "--help"):
            print(help_text)
        if (argv[1] == "--continue"):
            run_set.continue_run(dirname1, dirname2, range1, range2, executable=binary)
        if (argv[1] == "--no-run"):
            run_set.new_calc(dirname1, dirname2, range1, range2, variate_value, "echo not running " + binary, latex_symbol1, latex_symbol2)
        else:
            raise Exception("Unknown keyword " + argv[1])
        
        
        
"""
Now unused functions which might be helpfull to copypaste:

# Creates different qca-arrangement for different values of i
#   
def qca_configuration(i: int):
    if (i == 0):
        #conf_modification.replace_line("GRID_CONF", key="|DC|", 
        #                               replacement="|DC|", range=(57, 62))
        pass
    elif (i == 1):
        conf_modification.replace_line("GRID_CONF", key="|DC|", 
                                       replacement="| C|\n|D |", range=(57, 62))
    else:
        raise Exception("Out-of-bonds!")
    
    #rivi 57-62 "|DC|"


# Modify CONF file
# e.g. change temperature:
        conf_modification.old_bash_style_config_modifier(["<unused-arg>", "-f", "CONF", "--Temperature="+str(num1)])

"""
