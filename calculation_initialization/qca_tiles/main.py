#!/usr/bin/python3

"""
This script is for generating 2d cell arrangements from quantum cells. All required
infromation is read from GRID_CONF file, which must be located in the same folder
in which the script is run. After reading the config corresponding files ('QCA', 'QD', 
'PARTICLES' and 'paths/001/*.*.paths') are written. CONF -file is not generated 
by this program.

usage: main.py

Alpi Tolvanen, 2017, MIT-licence
"""

import os
import sys
if (os.path.dirname(os.path.realpath(__file__)) not in sys.path):  # add current dir in $PYTHONPATH
    sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import numpy as np
import read_config
import output



def main(argv):
    # reads config file GRID_CONF
    try:
        conf = read_config.read_config()
    except read_config._ConfExp as exp:
        print("Malformed GRID_CONF:", exp.args[0])
        print("exit")
        sys.exit(2)


    """ Arguments are only for testing purposes"""
    # test if unkown arguments are passed, currently there's only two different options to pass
    if len(argv) > 1:
        for arg in argv[1:]:
            if not (arg in ["-n", "--no-output", "-t", "--test"]):
                print("Unknown argument", arg, ", (exit)")
                sys.exit(2)
        
        if (("--test" in argv) or ("-t" in argv)):
            read_config.poor_mans_test_unit(conf)

        if (("--no_output" in argv) or ("-n" in argv)):
            print("-n|--no_output : No files are is written")
            return
        
    # writes to files 'QCA' 'QD' 'PARTICLES' and 'paths/001/*.*.paths'
    output.write_to_files(conf)

        


if __name__ == "__main__":
    main(sys.argv)
