"""
This module writes files corresponding files QCA QD PARTICLES and paths/001/*.*.paths 
using information from GRID_CONF.
"""
import os


def write_to_files(conf):

    write_PARTICLES(conf)

    write_QD(conf)

    write_QCA(conf)

    write_paths(conf)
    
    write_PSEUDO(conf)


def write_PARTICLES(conf):

    with open("PARTICLES", "w") as file:

        # all particles that are in tiling (list comprehension)
        gen_particles = [(key, conf.particles_dict[key]) \
                         for key in conf.particles_dict \
                         if conf.particles_dict[key].running_num != 0]
        gen_particles.sort(key=lambda x: x[1].index_num)

        lines = []

        for key, prt in gen_particles:
            lines.append(key + \
                         "\t"+str(prt.running_num)+\
                         "\t"+str(prt.charge)+\
                         "\t"+str(prt.mass)+\
                         "\t"+str(prt.param3)+\
                         "\t"+str(prt.param4)+"\n")

        file.writelines(lines)


def write_QD(conf):
    with open("QD","w") as file:

        qca_cell = None

        # finds first qca cell and uses that in QD
        # Todo: use the qca cell for that, not custom one
        for cell in conf.tiling_cells_vec:
            if cell.is_qca_cell:
                qca_cell = cell
                break

        lines = []

        for prt in qca_cell.particles:
            # automaticly assume that only ps particles form a cell to be tracked
            if (prt.symbol == "ps"):
                lines.append(str(prt.coordinate[0])+"\t"+str(prt.coordinate[1])+"\t"+str(prt.coordinate[2])+"\t\t"+"0.0 0.0 0.0\n")

        file.writelines(lines)


def write_QCA(conf):
    with open("QCA","w") as file:

        qca_cells = []

        # finds first qca cell and uses that in QD
        for cell in conf.tiling_cells_vec:
            if cell.is_qca_cell:
                qca_cells.append(cell)

        lines = []

        for cell in qca_cells:
            
            electrons = []
            for prt in cell.particles:
                if prt.symbol=="el":
                    electrons.append(prt)

            # what the hell is the following line supposed to be?
            # cell.position

            lines.append(str(electrons[0].number)+"\t"+str(electrons[1].number)+"\t"+str(cell.position[0])+
                         "\t"+str(cell.position[1])+"\t"+str(cell.position[2])+"\n")

        file.writelines(lines)


def write_paths(conf):

    if not os.path.exists("paths/001"):
        os.makedirs("paths/001")

    if not os.path.exists("paths/002_initial"):
        os.makedirs("paths/002_initial")

    for cell in conf.tiling_cells_vec:
        for prt in cell.particles:

            with open(("paths/001/"+prt.symbol+"."+str(prt.number)+".path"), "w") as file:
                file.write(str(cell.position[0]+prt.coordinate[0])+"\t"+str(cell.position[1]+prt.coordinate[1])+"\t"+str(cell.position[2]+prt.coordinate[2])+"\n")

            with open(("paths/002_initial/"+prt.symbol+"."+str(prt.number)+".path"), "w") as file:
                file.write(str(cell.position[0]+prt.coordinate[0])+"\t"+str(cell.position[1]+prt.coordinate[1])+"\t"+str(cell.position[2]+prt.coordinate[2])+"\n")


def write_PSEUDO(conf):
    with open("PSEUDO","w") as file:
        # all particles that are in tiling (list comprehensions are bad, aren't they?)
        gen_particles = [(key, conf.particles_dict[key]) \
                         for key in conf.particles_dict \
                         if conf.particles_dict[key].running_num != 0]
        gen_particles.sort(key=lambda x: x[1].index_num)
        
        el_idx = -1
        for i in range(len(gen_particles)):
            if gen_particles[i][0] == "el":
                el_idx = i
                break
        if el_idx == -1:
            input("Electron 'el' not found! If that's itended press [enter], else [^C]")
            #return
        
        lines = []
        
        for i in range(len(gen_particles)):
            if i != el_idx:
                # Soften 1 3 0      # what is that last zero?
                lines.append("Soften" + "\t" + str(el_idx+1) + "\t" + str(i+1) + "\t" + str(0) + "\n")
                
            
        file.writelines(lines)
