'''
Created on 20 Jun 2017
This is module for reading 2D-cell-generator config file "GRID_CONF", made to work with QCA pimc3 software

Licence 'do what you want', so MIT is good
Alpi Tolvanen 2017


Sorry about the code line lengths, I was using code margin at 120 characters.
And by the way, the incorrect usage is not tested. i.e no warranty for malformed GRID_CONF file!
'''

import math  # floor


def read_config():
    """
    This function reads config GRID_CONF-file in parts.
    :return:
    """
    lines = []        # to be stripped lines
    line_ind = 0    # current line to be read
    lines_ind = []  # corresponding line numbers (+1) with stripped and unstripped
    conf = configuration()

    with open("GRID_CONF", "r") as file:
        lines_read = file.readlines()

    lines, lines_ind = _strip_lines(lines_read)

    line_ind = define_grid(lines, line_ind, lines_ind, conf)

    line_ind = define_particles(lines, line_ind, lines_ind, conf)

    line_ind = define_cells(lines, line_ind, lines_ind, conf)

    line_ind = define_tiling(lines, line_ind, lines_ind, conf)

    #poor_mans_test_unit(conf)

    return conf


def _strip_lines(lines):
    """
    This function removes comments and empty lines from 'lines'
    :param lines: list of all lines of conf-file
    :return: lines_cpy: list of pure compact form of lines, this list is read in the module
    :return: lines_ind: indexes of lines in original un-cut list
    """
    lines_cpy = []
    lines_ind = []
    for line_idx, line in enumerate(lines):
        comment_idx = line.find("#")
        newline = ""
        if (comment_idx != -1):
            newline = line[0:comment_idx]
        else:
            newline = line
        newline = newline.strip()
        if (newline != ""):
            lines_cpy.append(newline)
            lines_ind.append(line_idx+1)
        if (newline == "END_OF_CONFIG"):
            break

    return lines_cpy, lines_ind


class configuration:
    """
    This is the data structure to be handed over to main program. Why class?, you might ask.
    Because of power of naming fields.
    """
    def __init__(self):
        self.sample_cells_vec = []  # vector<cell_struct> # this contains every different cell once e.g. [A,B,C,D]
        self.tiling_cells_vec = []  # vector<cell_struct> # this contains all cells used in tiling  e.g. [A,A,A,C,B,B]
        self.particles_dict = {}    # map<char,_general_particle_type>  # list of every particle
        self.a = -1.0
        self.d = -1.0
        self.l = -1.0


class _cell_struct:
    """
    This class acts as struct for quantum dot cell.
    This is used in two ways in this module.
        1. as sample of different cells (without position and numbering) (see sample_cells_vec)
        2. as actual cells  (see tiling_cells_vec)
    """
    def __init__(self, cell_type, letter, particles, is_qca_cell):
        self.cell_type = cell_type      # string "QCA_CELL"
        self.letter = letter            # char 'C'
        self.particles = particles      # vector<_inner_paricle_struct>
        self.is_qca_cell = is_qca_cell  # qca cell- polarization and fidelity tracking
        self.index_num = -1000          # will be added in tiling section
        self.position = [float('nan'), float('nan'), float('nan')]  # will be added in tiling section

    def generate_copy(self, conf):
        """
        This method returns valid copy of the cell with correct numbering of particles it includes
        :param conf: configure data struct -class thing
        :return: valig copy of itself
        """

        particles = []  # [x for x in cell_struct.particles ]
        for p in self.particles:
            # same particles, but with new running index with each one
            particles.append(_inner_paricle_struct(p.symbol, p.coordinate, conf, True))  

        cell = _cell_struct(self.cell_type, self.letter, particles, self.is_qca_cell)

        return cell


class _inner_paricle_struct:
    """
    This struct contains information of what particle is located where. See _general_particle_type
    and conf.particles_dict to see how symbols are linked to correspondin particle.
    """

    def __init__(self, symbol, coordinate, conf, count_particles):
        self.symbol = symbol            # string "el"
        self.coordinate = coordinate    # vector<float>(3)
        self.number = -1                # e.g el.2.path
        if count_particles:
            if symbol in conf.particles_dict:
                conf.particles_dict[symbol].running_num = conf.particles_dict[symbol].running_num + 1
                self.number = conf.particles_dict[symbol].running_num            # const int
            else:
                raise _ConfExp("Particle " + symbol + " not defined")


class _general_particle_type:
    """
        This struct contains particles' generic information (mass, charge), but not specific (like position, number)
        This file is read from Particles
    """
    def __init__(self, charge, mass, param3, param4, index_num):
        self.charge = charge        # float
        self.mass = mass            # float
        self.param3 = param3        # int
        self.param4 = param4        # int
        self.index_num = index_num  # int   # this is order of particles to be in PARTICLES-file, starts from one
        self.running_num = 0        # int running number which is incremented when new particleis created, count in the end


class _ConfExp(Exception):
    """
    This class works as a exception class which is thrown when error occurs in file reading
    (malformed file).
    """
    pass


def define_grid(lines, line_ind, lines_ind, conf):
    """
    This reads general grid size parameters of DEFINE_GRID
    :param lines:       stripped lines of config file
    :param line_ind:    index of current line under reading
    :param lines_ind:   correspoding lines in actual config file
    :param conf:        config data structure which is modified
    :return: line_ind   updated line under reading process
    """

    # first error checking in config-file that values are read in correct order
    # also finding the ending line index
    if (lines[0] != "DEFINE_GRID"):
        raise _ConfExp("Line "+str(lines_ind[0])+": DEFINE_GRID should be first non-empty line")  # Malformed GRID_CONF

    line_end_ind = 0
    for i in range(1, len(lines)):

        if (lines[i].find("DEFINE") == 0):
            if (lines[i].find("DEFINE_PARTICLES") != 0):
                raise _ConfExp("Line "+str(lines_ind[i])+": Expecting DEFINE_PARTICLES, got: " + lines[i])
            else:
                line_end_ind = i
                break

        if (len(lines[i].split()) != 2):
            raise _ConfExp("Line "+str(lines_ind[i])+": Two values expected from line, got: "+lines[i])

        if (i == len(lines)-1):
            raise _ConfExp("Line "+str(lines_ind[i])+": End of file reached, expected DEFINE_PARTICLES")


    for i in range(1, line_end_ind):
        # for reminder the line is like:
        # GRID_SIZE_a 1.0
        parts = lines[i].split()
        if (parts[0] == "GRID_SIZE_a"):
            conf.a = float(parts[1])
        elif (parts[0] == "GRID_SIZE_d" or parts[0] == "GRID_SIZE_l"):
            if (parts[0] == "GRID_SIZE_d"):
                conf.d = float(parts[1])
            elif (parts[0] == "GRID_SIZE_l"):
                conf.l = float(parts[1])
        else:
            raise _ConfExp("Line "+str(lines_ind[i])+": Unexpected variable: " + parts[0])

    if (conf.l < 0):  # unset
        conf.l = 2*conf.a + conf.d
    elif (conf.d < 0 and conf.l > 0):
        conf.d = conf.l - 2 * conf.a

    if (conf.a<0 or conf.d<0 or conf.l<0):
        raise _ConfExp("Line "+str(0)+": Not defined a and (d or l)")

    return (line_end_ind)


def define_particles(lines, line_ind, lines_ind, conf):
    """
    Reads particles from DEFINE_PARTICLES, this is like in PARTICLES -file but without particle amount
    :param lines:       stripped lines of config file
    :param line_ind:    index of current line under reading
    :param lines_ind:   correspoding lines in actual config file
    :param conf:        config data structure which is modified
    :return: line_ind   updated line under reading process
    """
    # first error checking that values are read in correct order
    line_end_ind = 0
    # error checking, finding the ending line index
    for i in range(line_ind+1, len(lines)):

        if (lines[i].find("DEFINE") == 0):
            if (lines[i].find("DEFINE_CELLS") != 0):
                raise _ConfExp("Line "+str(lines_ind[i])+": Expecting DEFINE_CELLS, got: " + lines[i])
            else:
                line_end_ind = i
                break

        if (len(lines[i].split()) != 5):
            raise _ConfExp("Line "+str(lines_ind[i])+": Five values expected from line, got:"+str(len(lines[i].split()))+"in",lines[i])

        if (i == len(lines)-1):
            raise _ConfExp("Line "+str(lines_ind[i])+": End of file reached, expected DEFINE_CELLS")

    # the reading itself
    for i in range(line_ind+1, line_end_ind):
        # for reminder the line is like:
        # el    -1.0    1.0     5    0
        parts = lines[i].split()
        particle = _general_particle_type(float(parts[1]), float(parts[2]), int(parts[3]), int(parts[4]), i-line_ind)
        conf.particles_dict[parts[0]] =  particle

    return (line_end_ind)


def define_cells(lines, line_ind, lines_ind, conf):
    """
    Reads DEFINE_CELLS-section, stores samples of cells in memory to be used in tiling-section
    :param lines:       stripped lines of config file
    :param line_ind:    index of current line under reading
    :param lines_ind:   correspoding lines in actual config file
    :param conf:        config data structure which is modified
    :return: line_ind   updated line under reading process
    """

    # first error checking that values are read in correct order
    line_end_ind = 0
    # error checking, finding the ending line index
    for i in range(line_ind+1, len(lines)):

        if (lines[i].find("DEFINE") == 0):
            if (lines[i].find("DEFINE_TILING") != 0):
                raise _ConfExp("Line "+str(lines_ind[i])+": Expecting DEFINE_TILING, got: "+ lines[i])
            else:
                line_end_ind = i
                break

        if (len(lines[i].split()) != 1 and len(lines[i].split()) != 2 and len(lines[i].split()) != 4):
            raise _ConfExp("Line "+str(lines_ind[i])+": One, two, or four value expected from line, got: "+str(len(lines[i].split()))+" in "+lines[i]+\
                           "\nCorrect forms: 'QCA_CELL:C' or 'CUSTOM:X BEGIN' or 'el 1.0 1.0 -1.0'")

        if ((len(lines[i].split()) == 1) and \
            (len(lines[i].split()[0].split(':')) != 2) and \
            (len(lines[i].split()[0].split(':')[1]) != 1) ):
            raise _ConfExp("Line "+str(lines_ind[i])+": Correct form: QCA_CELL:C \n yours: "+lines[i])

        if ((len(lines[i].split()) == 2) and \
            (len(lines[i].split()[0].split(':')) != 2) and \
            (len(lines[i].split()[1] != "BEGIN"))):
            raise _ConfExp("Line "+str(lines_ind[i])+": Correct form: CUSTOM:X BEGIN \n yours: "+lines[i])

        if (i == len(lines)-1):
            raise _ConfExp("Line "+str(lines_ind[i])+": End of file reached, expected DEFINE_TILING")

    # the reading itself
    line_ind = line_ind+1
    parts = lines[line_ind].split()
    while (line_ind != line_end_ind):
        # for reminder the line is like:
        # QCA_CELL:C

        if (len(parts) == 1):
            cell_parts = parts[0].split(':')
            if (cell_parts[0] == "QCA_CELL"):

                dot_a = _inner_paricle_struct("ps",[-conf.a/2, conf.a/2, 0],conf, False)
                dot_b = _inner_paricle_struct("ps",[conf.a/2, conf.a/2, 0],conf, False)
                dot_c = _inner_paricle_struct("ps",[conf.a/2, -conf.a/2, 0],conf, False)
                dot_d = _inner_paricle_struct("ps",[-conf.a/2, -conf.a/2, 0],conf, False)
                electron1 = _inner_paricle_struct("el",[conf.a/2, conf.a/2, 0],conf, False)
                electron2 = _inner_paricle_struct("el",[-conf.a/2, -conf.a/2, 0],conf, False)

                particle_vec = [dot_a, dot_b, dot_c, dot_d, electron1, electron2]

                cell = _cell_struct("QCA_CELL", cell_parts[1],particle_vec, True)

                conf.sample_cells_vec.append(cell)

            elif (cell_parts[0] == "DRIVER"):

                driver_a = _inner_paricle_struct("ps",[-conf.a/2, conf.a/2, 0],conf, False)
                driver_b = _inner_paricle_struct("mn",[conf.a/2, conf.a/2, 0],conf, False)
                driver_c = _inner_paricle_struct("ps",[conf.a/2, -conf.a/2, 0],conf, False)
                driver_d = _inner_paricle_struct("mn",[-conf.a/2, -conf.a/2, 0],conf, False)

                particle_vec = [driver_a, driver_b, driver_c, driver_d]

                cell = _cell_struct("DRIVER", cell_parts[1],particle_vec, False)

                conf.sample_cells_vec.append(cell)

            else:
                raise _ConfExp("Line "+str(lines_ind[line_ind])+": Unexpected variable, valid: {QCA_CELL,DRIVER}, yours: " + cell_parts[0])

            line_ind += 1
            parts = lines[line_ind].split()
            continue

        elif (len(parts) == 2):
            cell_name = parts[0].split(':')[0]
            cell_letter = parts[0].split(':')[1]

            particle_vec = []

            line_ind += 1
            parts = lines[line_ind].split()
            # inner line-counting loop for cell's particles' coordinates
            while (parts[1] != "END"):

                particle = _inner_paricle_struct(parts[0], [float(parts[1]), float(parts[2]), float(parts[3])],conf, False)

                particle_vec.append(particle)

                line_ind += 1
                parts = lines[line_ind].split()

                if ((parts[1] != "END") and (len(parts) != 4)):
                    raise _ConfExp("Line "+str(lines_ind[line_ind])+": Expected something like: 'el 1.0 1.0 -1.0' got '"+lines[line_ind]+"'")
                elif ((parts[1] == "END") and (parts[0] != cell_name+":"+cell_letter)): # begin and end name must be identical
                    raise _ConfExp("Line "+str(lines_ind[line_ind])+": Begin and end names do not match: "+parts[0]+" versus: "+cell_name+":"+cell_letter)

            if (cell_name == "QCA_TRACKED"):
                cell = _cell_struct(cell_name, cell_letter, particle_vec, True)
            elif (cell_name == "CUSTOM"):
                cell = _cell_struct(cell_name, cell_letter, particle_vec, False)
            else:
                raise _ConfExp("Line "+str(lines_ind[line_ind])+": Cells type must either be {QCA_TRACKED,CUSTOM}, yours: "+cell_name)

            conf.sample_cells_vec.append(cell)

            line_ind += 1
            parts = lines[line_ind].split()

    return (line_end_ind)


def define_tiling(lines, line_ind, lines_ind, conf):
    """
    Reads from DEFINE_TILING-section, every character is matched with corresponding cell.
    :param lines:       stripped lines of config file
    :param line_ind:    index of current line under reading
    :param lines_ind:   correspoding lines in actual config file
    :param conf:        config data structure which is modified
    :return: line_ind   updated line under reading process
    """
    # first error checking that values are read in correct order
    line_end_ind = 0
    x_length = len(lines[line_ind +1])
    # error checking, finding the ending line index
    for i in range(line_ind+1, len(lines)):

        if (lines[i].find("END_OF_CONFIG") == 0):
            line_end_ind = i
            break

        if not ((lines[i][0] == "|") and (lines[i][-1] == "|")):
            raise _ConfExp("Line "+str(lines_ind[i])+": Tiling line must start and end with '|'-character like so: | CDDD |, got: "+(lines[i]))

        if (len(lines[i]) != x_length):
            raise _ConfExp("Line "+str(lines_ind[i])+": Tiling line's lengths are different!")

        if (i == len(lines)-1):
            raise _ConfExp("Line "+str(lines_ind[i])+": End of file reached, expected END_OF_CONFIG")

    y_length = line_end_ind-1 - line_ind

    # x is to rigth, y up
    center_x = math.floor((x_length-2)/2)
    center_y = math.floor(y_length/2)

    #cell_numbers_dict = {}  # map<char,int>

    # the reading itself
    for j in range(x_length-2):
        x_coord = (j - center_x) * conf.l

        for i in range(line_ind+1, line_end_ind):
            y_coord = ((line_end_ind-1-i) - center_y) * conf.l
            # for reminder the line without '| |' is like:
            # line = lines[i][1:-1]
            char_of_cell = lines[i][j+1]

            if (char_of_cell == " "):
                continue
            else:
                # list of all cells corresponding of that character
                sample_cell_vec = [cell_obj for cell_obj in conf.sample_cells_vec if cell_obj.letter == char_of_cell]

                if (len(sample_cell_vec) == 0):
                    raise _ConfExp("Line "+str(lines_ind[i])+": No matching cell for character found in tiling: "+char_of_cell)

                new_cell = sample_cell_vec[0].generate_copy(conf)   # this method takes in account running particle numbers
                new_cell.index_num = len(conf.tiling_cells_vec)     # every cell has different index number, starting from 0
                new_cell.position = [x_coord, y_coord, 0]           # centerpoint of cell
                conf.tiling_cells_vec.append(new_cell)

    return line_end_ind

def poor_mans_test_unit(conf):
    """
    I didn't see it worth the price to make unit test for something of this scale, so this is the
    way to test the module.
    :param conf:
    :return:
    """
    print("\n---Print conf members---")
    for x in vars(conf):
        print(x)
    print("-----\n")

    print("\n---Print cell_vec and particles---")
    for x in conf.sample_cells_vec:
        print("    ", vars(x))
        for y in x.particles:
            print("        ", vars(y))
        print("    -----")
    print("-----\n")

    print("\n---Print only cell char and cell's position---")
    for x in conf.tiling_cells_vec:
        print("    ", x.letter, x.position)
    print("-----\n")

    print("\n---Print particle dict---")
    for key in conf.particles_dict:
        print(key, vars(conf.particles_dict[key]))
    print("-----\n")
