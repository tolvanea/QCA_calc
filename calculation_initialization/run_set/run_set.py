import os
#from distutils.dir_util import copy_tree
import shutil
import subprocess
import shlex
import time
from sys import argv
import json
import math

import miscellaneous.change_priority as change_priority

help_text = """
This program is supposed to run everyhing in calulation part
1. generate required files for all different calculations
2. run pimc3
"""

def new_calc(dirname1, dirname2, range1, range2, fun, executable, latex_symbol1, latex_symbol2):
    initial_error_checking()
    store_run_info(dirname1, dirname2, range1, range2, latex_symbol1, latex_symbol2)
    generate_cases_and_run(dirname1, dirname2, range1, range2, fun, executable)
    

def initial_error_checking():
    par_dir_name = os.path.basename(os.getcwd())
    list_dir = os.listdir()
    
    if not (("CONF" in list_dir) and ("GRID_CONF" in list_dir)):
        raise Exception("You must run this in directory 'sample' with 'CONF' and 'GRID_CONF'")
    if par_dir_name != "sample":
        raise Exception("Upper folder name must be 'sample'")

def generate_cases_and_run(dirname1, dirname2, range1, range2, variate_value_fun, executable="pimc3"):
    
    dir_labels1, dir_labels2 = dir_number_formatting(dirname1, dirname2, range1, range2)
    
    os.chdir("..")
    
    """subprocess.Popen(["konsole", "--noclose", "--workdir", 
                    os.getcwd(), "-e", 'echo ' + 'This is the main work directory. After the ' +
                    'computation has finished run here\n' + 
                    '$ python3 -m plotting_1'],
                    cwd=os.getcwd())
    """
    for i, num1 in enumerate(range1):
        dir1 = dir_labels1[i]
        if not os.path.exists(dir1):
            os.makedirs(dir1)
        else:
            raise Exception("Directory "+str(dir1)+" already exists! Use --continue")
        
        os.makedirs(dir1+"/sample")
        shutil.copyfile("sample/CONF", dir1 + "/sample/CONF")
        shutil.copyfile("sample/GRID_CONF", dir1 + "/sample/GRID_CONF")
        
        # change directory
        os.chdir(dir1)
        
        for j, num2 in enumerate(range2):
            dir2 = dir_labels2[j]
            os.makedirs(dir2)
            
            shutil.copyfile("sample/CONF", dir2 + "/CONF")
            shutil.copyfile("sample/GRID_CONF", dir2 + "/GRID_CONF")
        
            os.chdir(dir2)
            if True:  # Do the thing (i.e. run pimc3)
                
                variate_value_fun(num1, num2)
                
                subprocess.Popen(["konsole", "--noclose", "--new-tab", "--workdir", 
                                  os.getcwd(), "-e", 
                                  "stdbuf -o 0 " + executable + " 2>&1 | tee stdout.txt"],
                                 cwd=os.getcwd())
                
            os.chdir("..")
            
        shutil.rmtree('sample')
        
        os.chdir("..")
        
        time.sleep(2)
        change_priority.main(["asdf", executable, "low"])

def run_command(command):
    # WARNINg method not tested yet! Also not used anywhere
    #https://www.endpoint.com/blog/2015/01/28/getting-realtime-output-using-python
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        #if output:
        #print(output.strip())
        hs = open("stdout.txt","a")
        hs.write(output)
        hs.close() 
    rc = process.poll()
    return rc
    
    

def continue_run(dirname1, dirname2, range1, range2, executable="pimc3"):
    
    raise Exception("Function is old, not tested and probably broken")
    #########################################################
    
    with open('sample/configuration_info.json', 'r') as file:
        n, n, n, n, dir_labels1, dir_labels2 = json.load(file)
    
    for num1 in range1:
        dir1 = dir_labels1[i]
        
        os.chdir(dir1)
        
        for num2 in range2:
            dir2 = dir_labels2[j]
            
            os.chdir(dir2)
            subprocess.Popen(["konsole", "--noclose", "--new-tab", "--workdir", 
                              os.getcwd(), "-e", 
                              #"stdbuf -o 0 " + executable + " 2>&1 | tee stdout.txt"], # shold store stdout, doesn't work
                              executable],
                             cwd=os.getcwd())
            
            os.chdir("..")
            
        os.chdir("..")
        
        time.sleep(2)
        change_priority.main(["asdf", executable, "low"])


def dir_number_formatting(dirname1, dirname2, range1, range2):
    """
    These will be directory names to calculations
    Direcrtory names are like var_X0.0200
    
    When I first made my octave scipts, they were a hack. Now when I use them, 
    they are still the same hack. 
    """
    labels = [[], []]
    
    # number formatting in string with constant length
    for i, range12 in enumerate([range1, range2]):
        max_len = 1
        for num in range12:
            l = len(str(num))
            max_len = max(l, max_len)
        
        num_decimals = min(4, max_len)
        #num_decimals = 6
        
        # pythons format is broken: it seems impossible to format float in given space
        #stringify = lambda f,l: ("{:0"+str(l)+"f}").format(round_sigfigs(f, sig_figs=l))
        stringify = lambda f,l: format_float_n_decimals(f,l)
        
        labels_str = list(map(lambda f: stringify(f, num_decimals), range12))
        
        
        # test whether 5 decimals is enough to identify dir labels
        if len(list(set(labels_str))) < len(labels_str):
            labels_str = map(lambda f: stringify(f, max_len), range12)
        
        labels[i] = labels_str
    
    # add string to number
    for i, range12 in enumerate([range1, range2]):
        
        if i == 0:
            dirname = dirname1
        else:
            dirname = dirname2
        
        for j in range(len(range12)):
            labels[i][j] = dirname + labels[i][j]
        
    return labels[0], labels[1]


def store_run_info(dirname1, dirname2, range1, range2, latex_symbol1, latex_symbol2):
    
    dirlabels1, dirlabels2 = dir_number_formatting(dirname1, dirname2, range1, range2)
    
    with open("configuration_info.json", "w") as file:
        json.dump([dirname1, dirname2, list(range1), list(range2), dirlabels1, dirlabels2,\
                    latex_symbol1, latex_symbol2], file)


def format_float_n_decimals(f,n):
    "gosh this can be hard to implement!"
    if f > 1: # case 12.1234
        num_ints = round(math.log10(f)+1)
    elif (f <= 1) and (-1 <= f): # 0.001234
        num_ints = 1
    elif f < 0: # case 
        num_ints = round(math.log10(-f)+1)
    else:
        raise Exception("Wut?")
    
    
    num_decs = n - num_ints -1
    if num_decs < 0:
        num_decs = 0
    
    form = ("{:"+str(num_ints)+"."+str(num_decs)+"f}").format(f)
    return form


# http://code.activestate.com/recipes/578114-round-number-to-specified-number-of-significant-di/
def round_sigfigs(num, sig_figs):
    """Round to specified number of sigfigs.

    >>> round_sigfigs(0, sig_figs=4)
    0
    >>> int(round_sigfigs(12345, sig_figs=2))
    12000
    >>> int(round_sigfigs(-12345, sig_figs=2))
    -12000
    >>> int(round_sigfigs(1, sig_figs=2))
    1
    >>> '{0:.3}'.format(round_sigfigs(3.1415, sig_figs=2))
    '3.1'
    >>> '{0:.3}'.format(round_sigfigs(-3.1415, sig_figs=2))
    '-3.1'
    >>> '{0:.5}'.format(round_sigfigs(0.00098765, sig_figs=2))
    '0.00099'
    >>> '{0:.6}'.format(round_sigfigs(0.00098765, sig_figs=3))
    '0.000988'
    """
    if num != 0:
        return round(num, -int(math.floor(math.log10(abs(num))) - (sig_figs - 1)))
    else:
        return 0  # Can't take the log of 0

        
if __name__ == "__main__":
    
    if (len(argv) > 1) and (argv[1] == "--help"):
        print(help_text)
    else:
        print("Do not run this directly")

