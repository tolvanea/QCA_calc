# QCA-system calculations

This project contains many different programs that automatize simulations, calculate classical energies and plot figures that were used in bachelor thesis:

[Kvanttipistesoluautomaattien mallintaminen polkuintegraali-Monte Carlo -menetelmällä](https://gitlab.com/tolvanea/kandi).

Simple QCA-systems (quantum dot cellular automata) are modeled with PIMC (path integral Monte Carlo) and classical methods. All code is strongly build around PIMC-simulation program [https://gitlab.com/qcad.fi/pimc3](https://gitlab.com/qcad.fi/pimc3), which is developed in Electronic Structure Theory Group in TUT. 

About half of this code is written in summer 2017, when workging as a research assistant in electronic structure group. The other half of code is written in winter 2017-2018 along the bachelor thesis.

## Directories:
* ```calculation_initialization -``` - automatizations of [pimc3](https://gitlab.com/qcad.fi/pimc3) suited for QCA sensitivity analysis 
* ```result_analysis            -``` - classical energy calculations and figure plotting
* ```templates                  -``` - initial configurations of calculations used in Bsc. thesis
* ```miscellaneous              -``` - general help functions used in programs

## Few words about this project

All programs are made with Python3.6 + Anaconda (there also exists one octave-script). It might be difficult to get these scripts and programs working correctly, and there are configurations to be made. However this file attempts to guide setting all up, and demonstrate the correct usage of programs.

This project is set of separate scripts that are designed some very specific task in mind. So there is no general, easy-to-use, all-in-one -program, but instead multible scripts for individual cases. Code is not necessarily easy to use or easy to maintain.

All code is (unoffically) under MIT-licence, and is therefore free to use in any ways.


## Reproducing results of Bsc.-thesis
Demonstrate setup and the basic usage.
### Setting up

(These instructions are for Linux-based operating systems only.)

* Install Python 3.6 via Anaconda (packages used: Matplotlib and Numpy).
* Install Octave version 4.0+.
* Add required modules to $PYTHONPATH by appending the following line in file `~/.bashrc`:

```
        export PYTHONPATH="${PYTHONPATH}:/full/path/to/QCA_calc/"
```

* This following addition should be automated in the scripts, but by making it by hand is more explicit. add path to ~/.octaverc:

```        
        addpath("/full/path/to/result_analysis/read_calc_data")  `
```

* Also pimc3-binary should be runnable from terminal

``` 
        export PATH="$PATH:/full/path/to/pimc3/pimc3_bin"
``` 

### Running one of calculation sets
This describes an example how to repeat one Bsc.-thesis calculation, read results and plot the figures.

1. Change current directory desired calculation set: 
    
    `cd QCA_calc/templates/1_perus/sample/`
    - This specific directory `1_perus` contains initial parameters for case: _2 sidewise coupled QCA cells_ (kyljittäinen kytkentä). 
    - If needed, modify inital conditions in ```CONF``` or ```GRID_CONF```
    - If needed, modify the rule how system is varied in: ```variate_and_run.py``` 
        - Do not forget to change its the `pimc3`-binary name to correct one.

2. Start all calulations by running the python file

    ```python3 variate_and_run.py``` 

    * In this directory (`1_perus`) it starts 33 different instances of pimc3-program with varying parametes T = {100, 200, 300K}, P = {-1,...+1}. Normal desktop computer freezes for a day.
    * For each of 33 different pimc-simulations, it:
        * creates direcotry, copies CONF, and GRID_CONF, modifies them as commanded in `variate_and_run`
        * generate everything required by a pimc3-simulation
            * create QCA-cell-array by placing quantum dots in correct coordinates
            * keep book of particle count etc..
            * i.e write files: `paths/001/{...} PARTICLES, PSEUDO, QCA, QD`...
        * generates coordinates that are used by classical calculation scripts
            * write `paths/002_initial`


3. After the calculation has finished go back in `templates/1_perus` -directory 

    ``` cd .. ```

4. Run analyzation and plot figures

    ```python3 plot_it.py```

    which:
    * reads PIMC-results-data
    * does all the classical calculations
    * saves both PIMC, and classical calculations in directory `1_perus/results_data/`
    * plots figures
    
    Directory's `plot_it.py` calls the correct plotting function:
    * `1_perus, 2_not       --> plot_side_and_corner.py`
    * `3_AND, 4_OR          --> plot_AND_and_OR.py`
    * `5_suora, 6_NOT       --> plot_wire_or_NOT.py`

        
If everything worked correctly, results presented in bachelor thesis should be obtained.

