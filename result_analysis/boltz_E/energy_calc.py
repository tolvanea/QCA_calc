#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

"""
This file contains functions that compute electron calculations. 
"""
import sys
from typing import Tuple, List
import math
import numpy as np
import time


def new_calc_energy(cells, fixed_particles, dielectric=1.0):
    """
    Uses new struct from 'new_read_paths()' and less code to calculate energy for that
    particle combination

    This function will be used in combinatory calulations 'new_electron_combinations()'.
    
    Args:
        cells:              cell-structure, contains 4+2 particles, (assuming nanometers and atomic units)
        fixed_particles:    particles that are exluded from cells,
        dielectric:         
    
    Return:
        configuration energy in electron volts
        
    Todo:
        optimize this with some python magic, or re-write in c++. This is the function in which most the of execution time is being spent.
    """
    # type of particle      0     1     2      3      4
    #    np.ndarray       [crd1, crd2, crd3, charge, radius]
    

    # constanst SI
    eps0 = 8.854187817e-12
    pi = 3.1415926535
    k = 1/(4*pi*eps0*dielectric) # 1/ (4*pi*eps)
    q_e = 1.60217662e-19;

    # convert all particles to one big list
    all_particles : List[np.ndarray] = fixed_particles[:] 
    # I dissaprove this list-copying syntax of python!
    for cell in cells:
        all_particles.extend(cell)


    Et = 0.0
    for i1, prt1 in enumerate(all_particles[:-1]):
        for i2, prt2 in enumerate(all_particles[i1+1:]):
            dist = math.sqrt((prt1[0]-prt2[0])**2+(prt1[1]-prt2[1])**2+(prt1[2]-prt2[2])**2);

            larger_radius = max(prt1[4], prt2[4])
            #  particle inside quantum dot
            if (dist < larger_radius):
                Et += k*prt1[3]*prt2[3]/(2*larger_radius) * (3 - dist**2/larger_radius**2) * (q_e**2 / (1e-9));
            # neither is quantum dot, OR they are far enough not to matter
            else:
                Et += k * prt1[3] * prt2[3] / (dist) * (q_e**2 / (1e-9)); # last term is for units


    return Et/q_e


class Energy_levels:
    """
    This class is tool used in 'confine_cell_electron_combinations' and 'flip_only_electron_combinations'.
    It calculates the energy, and keeps book of lowest energy states.
    """
    def __init__(self, fixed_particles, dielectric):
        self.count = 0;
        self.all_energies = []
        self.all_configurations = []
        self.fixed_particles : List[np.ndarray] = fixed_particles
        self.dielectric = dielectric

    def store_energy(self, cells):
        
        self.all_configurations

        energy = new_calc_energy(cells, self.fixed_particles, self.dielectric)
        self.all_energies.append(energy)

        self.all_configurations.append(cells)
        
        self.count += 1


def correct_energies(top_30_E, top_30_cells_conf, fixed_particles, dielectric=1.0):
    """
    This function tries to find energy minimum for electrons in QD. I.e electrons are moved from
    the exact center of QD. Energy minimizes when electrons are more in opposing corners QCA cell.
    This algorithm moves one electron at a time to its near potential minimum, and repeats this few
    rounds.
    """
    num_iterations = 3 # change to greater values for better accuracy, default 3

    QD_radius = top_30_cells_conf[0][0][2][4]

    def find_potential_minimum(cells, fixed_particles_, prt, QD_radius, dielectric, accuracy):
        # this is the following array: [-1, 0.1, 0, 0.1, 1], for better accuracy change 2->3
        # TODO CHANGE BACK TO 2
        log_range = np.concatenate((-np.logspace(-1,0,2), np.array([0.0]), np.logspace(-1,0,2)), axis=0)
        search_grid = log_range * QD_radius*0.3 * accuracy**2

        x_ax = prt[0] + search_grid #np.linspace(prt[0]-QD_radius, prt[0]+QD_radius,5)
        y_ax = prt[1] + search_grid #np.linspace(prt[1]-QD_radius, prt[1]+QD_radius,5)

        min_E = new_calc_energy(cells, fixed_particles_+[prt], dielectric)
        min_E_x_y = np.array([prt[0],prt[1]])
        for y in y_ax:
            for x in x_ax:
                prt_try = np.array([x,y,*prt[2:]])
                E_try = new_calc_energy(cells, fixed_particles_+[prt_try], dielectric)
                if E_try < min_E:
                    min_E = E_try
                    min_E_x_y = prt_try[0:2]

        return min_E_x_y

    new_top_30_E = list(top_30_E)
    new_top_30_cells_conf = list(top_30_cells_conf)

    print("Progress on energy corrections ( /30) ")

    start_time = time.time()


    none_idx = -1 # after this there are no confiigurations left in 30 items, and they are marked as None

    # fix energies for every cell-configuration
    for conf_idx, cells_conf in enumerate(new_top_30_cells_conf):

        print(conf_idx+1," ", sep="", end="")
        sys.stdout.flush()

        if cells_conf is None:
            none_idx = conf_idx
            break

        new_fixed = fixed_particles[:]
        new_cells = cells_conf[:]

        # do it iteratively few times, hopefully it converges
        for iteration in range(num_iterations):

            # fix fixed_particles energies, (do that iteratively for each particle)
            for i,prt in enumerate(new_fixed):
                #if electron, do move
                if math.isclose(prt[3], -1.0, rel_tol=1e-4) and math.isclose(prt[4], 0.0, rel_tol=1e-4):
                    new_fixed_pop_prt = new_fixed[0:i] + new_fixed[i+1:]
                    x_y = find_potential_minimum(new_cells, new_fixed_pop_prt, prt, QD_radius, dielectric, 1-iteration/num_iterations)
                    new_fixed[i] = np.array([*x_y, *prt[2:]])

            # fix cell energies, (do that iteratively for each particle)
            for cell_idx, cell in enumerate(new_cells):

                for prt_idx, prt in enumerate(cell):
                    #if electron, do move
                    if math.isclose(prt[3], -1.0, rel_tol=1e-4) and math.isclose(prt[4], 0.0, rel_tol=1e-4):
                        new_cell_pop_prt = cell[0:prt_idx] + cell[prt_idx+1:]
                        new_cells_pop_prt = new_cells[0:cell_idx] + [new_cell_pop_prt] + new_cells[cell_idx+1:]
                        x_y = find_potential_minimum(new_cells_pop_prt, new_fixed, prt, QD_radius, dielectric, 1-iteration/num_iterations)
                        new_cells[cell_idx][prt_idx] = np.array([*x_y, *prt[2:]])

        new_top_30_E[conf_idx] = new_calc_energy(new_cells, new_fixed, dielectric)
        new_top_30_cells_conf[conf_idx] = new_cells

    end_time = time.time()
    print("\nTime elapsed in energy correction:", end_time - start_time,"\n")


    if none_idx < 0: # there are all 30 confs
        # sort arrays, (name zip is reserved --> zip_0)
        zip_0 = list(zip(new_top_30_E, new_top_30_cells_conf))
        zip_0.sort(key=lambda x: x[0])
        # unzip
        new_sorted_E, new_sorted_conf = zip(*zip_0)
        # tupple -> list
        new_sorted_E = list(new_sorted_E)
        new_sorted_conf = list(new_sorted_conf)
    else:
        # sort arrays
        parital_E = new_top_30_E[0:none_idx]
        partial_conf = new_top_30_cells_conf[0:none_idx]

        zip_parital = list(zip(parital_E, partial_conf))
        zip_parital.sort(key=lambda x: x[0])
        # unzip
        sorted_E_partial, sorted_conf_partial = zip(*zip_parital)
        # tupple -> list
        sorted_E_partial = list(sorted_E_partial)
        sorted_conf_partial = list(sorted_conf_partial)

        new_sorted_E = sorted_E_partial + new_top_30_E[none_idx:]
        new_sorted_conf = sorted_conf_partial + new_top_30_cells_conf[none_idx:]


    return new_sorted_E, new_sorted_conf, new_fixed


def sort_by_energies(energies, confs):
    # sort arrays, (name zip is reserved --> zip_0)
    zip_0 = list(zip(energies, confs))
    zip_0.sort(key=lambda x: x[0])
    # unzip
    new_sorted_E, new_sorted_conf = zip(*zip_0)
    # tupple -> list
    new_sorted_E = list(new_sorted_E)
    new_sorted_conf = list(new_sorted_conf)
    return new_sorted_E, new_sorted_conf
