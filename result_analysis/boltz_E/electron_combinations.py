#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

import sys
import numpy as np
import time
import math
from typing import Tuple, List

from . import energy_calc


def choose_checked_states(cells, fixed_particles, dielectric, check_states):
#            -> Tuple[List[float], List[List[np.ndarray[np.ndarray]]]]:
    """
    Chooses the correct alorithm depending on what states are going to be permutated
    """
    # Return type for 'all_configurations'
    # configurations:                   List[List[np.ndarray[np.ndarray]]]
        # configuration                 List[np.ndarray[np.ndarray]]
            # cell                      np.ndarray[np.ndarray]
                # particle              np.ndarray
                    # x, y, z, charge, smthing...
                    
    print_text = ""
    plot_text = ""
    
    if check_states == "choose-best":
        num_cells = len(cells)
        
        if num_cells < 4+1:
            check_states = "permute_cell"
        else:
            check_states = "flip_only"
    
    if (check_states == "permute_all"):
        all_energies, all_configurations = all_electron_combinations(
                    cells, fixed_particles, dielectric)
        print_text = "Top energy levels, check all possible states"
        plot_text = "permute all combinations"

    elif (check_states == "permute_cell"):
        all_energies, all_configurations = confine_cell_electron_combinations(
                    cells, fixed_particles, dielectric)
        print_text = "Top energy levels, confine electrons inside cell"
        plot_text = "electrons confined inside cell"
    
    elif (check_states == "flip_only"): # check less cell states, flip only
        all_energies, all_configurations = flip_only_electron_combinations(
                    cells, fixed_particles, dielectric)
        print_text = "Top energy levels, allow flip only"
        plot_text = "allowing flip permutation only"
        
    else:
        raise Exception("Unknown option for --check: " + check_states)
    
    print(print_text)

    return all_energies, all_configurations



def all_electron_combinations(cells, fixed_particles, dielectric):
    """
    Calulates all compinations of electrons in quantum dots
    Electrons will not be used other than reference to count and properties.
    This part of code requires that there are two electrons in four-QD-cell.
    """
    Exception("must be two electrons in four-QD-cell")

    def convert_cells_to_initial_bool_list(cells):
        """
        This function converts cells to the initial state of list[bool], that can be passed to functions which
        compute permutations.
        """
        num_electrons = len(cells)*2

        qdots : List[np.ndarray] = []
        for cell in cells:
            qdots.extend(cell[2:])


        num_holes = len(cells)*2

        initial_state = [True for i in range(num_electrons)] + [False for i in range(num_holes)]
        return initial_state, qdots

    def convert_bool_list_to_cells(all_states, qdots, el_charge, el_radius):
        """
        :param: all_states  boolean values (T/F) for whether the corresponding qdot has a electron in it
        :param: qdots       quantum dots, corresponds the above in order
        """
        cells = []
        for cell_itr in range(len(all_states)//4):
            lower = cell_itr * 4
            upper = (cell_itr+1) * 4
            cell = convert_bool_list_to_cell(all_states[lower:upper], qdots[lower:upper], el_charge, el_radius)
            cells.append(cell)

        return cells


    def permute_reqursive(leading_QDs, trailing_QDs, E_calc, qdots, el_charge, el_radius):
        """
        This function permutes all combinations of states True and False. Used in permuting single
        cell's different electron configurations. Initial state is one in which all first ones are
        True and rest falste, like so: TTTFFFF

        :param leading_QDs: list[bool]
        :param trailing_QDs: list[bool]
        :param list_of_states: list[list[bool]] all states that have been iterated
        """
        cells = convert_bool_list_to_cells(leading_QDs+trailing_QDs, qdots, el_charge, el_radius)
        E_calc.store_energy(cells)

        if len(trailing_QDs) == 0:
            print("Progress permutation ( /16): ", sep="",end="")
        if len(trailing_QDs) == 5:
            print("#",sep="",end="")
            sys.stdout.flush()


        if (sum(leading_QDs) == 0):
            return
        else:
            for null in range(100):
                l = len(leading_QDs)
                # determine outer indice
                outer_ind = -1
                for ir in range(l-1,-1,-1):
                    if (leading_QDs[ir] == True):
                        outer_ind = ir
                        break

                if (outer_ind < l-1):
                    # move outer one right
                    leading_QDs[outer_ind] = False
                    leading_QDs[outer_ind+1] = True
                    permute_reqursive(leading_QDs[0:outer_ind+1], leading_QDs[outer_ind+1:] + trailing_QDs,
                                E_calc, qdots, el_charge, el_radius)
                else:
                    return
            raise Exception("Something's wrong, recursion did not return")


    E_calculator = energy_calc.Energy_levels(fixed_particles, dielectric)
    initial_state, qdots = convert_cells_to_initial_bool_list(cells)

    el_charge = cells[0][0][3] # obviously -1.0
    el_radius = cells[0][0][4] # obviously 0.0, but newer can't be sure!

    start_time = time.time()

    permute_reqursive(initial_state, [], E_calculator, qdots, el_charge, el_radius)

    end_time = time.time()

    print("\n","old_electron_combinations all possible states")
    print("Time elapsed:", end_time - start_time, "count", E_calculator.count,"\n")

    return E_calculator.all_energies, E_calculator.all_configurations


def convert_bool_list_to_cell(bool_state, qdots, el_charge, el_radius):
    electrons = []
    for i, qdot in enumerate(qdots):
        if bool_state[i]:
            coord = qdot[0:3]
            electrons.append(np.array([*coord, el_charge, el_radius]))
    cell = electrons+qdots
    return cell


def confine_cell_electron_combinations(cells, fixed_particles, dielectric):
    """
    This function finds the lowest energy within qca cells.
    Assumption: the electrons does not leave the cell they are in, (i.e. 2 electrons per cell in normal cases)

    :param cells: list of list of np.ndarrays   --  cells -> particles_of_cell -> coodrinates_of_particle
    """

    def permute_cell_reqursive(leading_QDs, trailing_QDs, list_of_states): # sub_list, remaider
        """
        This function permutes all combinations of states True and False. Used in permuting single
        cell's different electron configurations. Initial state is one in which all first ones are
        True and rest falste, like so: TTTFFFF

        :param leading_QDs: list[bool]
        :param trailing_QDs: list[bool]
        :param list_of_states: list[list[bool]] all states that have been iterated
        """
        list_of_states.append(leading_QDs+trailing_QDs)

        if (sum(leading_QDs) == 0):
            return
        else:
            for null in range(100):
                l = len(leading_QDs)
                # determine outer indice
                outer_ind = -1
                for ir in range(l-1,-1,-1):
                    if (leading_QDs[ir] == True):
                        outer_ind = ir
                        break

                if (outer_ind < l-1):
                    # move outer one right
                    leading_QDs[outer_ind] = False
                    leading_QDs[outer_ind+1] = True
                    permute_cell_reqursive(leading_QDs[0:outer_ind+1], leading_QDs[outer_ind+1:] + trailing_QDs, list_of_states)
                else:
                    return
            raise Exception("Something's wrong, recursion did not return")

    def permute_single_cell(cell):
        """
        This function takes one cell, and then converst its particle-data understood by
        permute_cell_reqursive. Afterwards it converts 'list_of_states' back to cell structure,
        and returns the list cells with different electron-combinations.

        This part of code requires that there are two electrons in 4-QD-cell.
        """
        el_charge = cell[0][3]
        el_radius = cell[0][4]

        def convert_cell_to_initial_bool_list(cell):
            """
            This function converst cell to the initial state of list[bool], that can be passed to functions which
            compute permutations.
            """
            num_electrons = 2

            num_holes = 2 #len(cell) - 2*num_electrons
            qdots = cell[num_electrons:]

            initial_state = [True for i in range(num_electrons)] + [False for i in range(num_holes)]
            return initial_state, qdots


        initial_state, qdots = convert_cell_to_initial_bool_list(cell)

        list_of_states = [] # list[list[bool]]

        permute_cell_reqursive(initial_state, [], list_of_states)

        # now we have list_of_states in bool format

        # convert bools to cell
        states_of_cell = []
        for state_bool_list in list_of_states:
            cell_state = convert_bool_list_to_cell(state_bool_list, qdots, el_charge, el_radius)
            states_of_cell.append(cell_state)

        return states_of_cell

    def permute_cells_recursive(E_calc, leading_cells, trailing_cells):

        if (len(trailing_cells) == 0):
            print("Permutation progress (/36) ",end="")
        elif (len(trailing_cells) == 2):
            print("#",end="")
            sys.stdout.flush()

        if (len(leading_cells) == 0):
            E_calc.store_energy(trailing_cells)
            return
        else:
            for permutation_of_cell in permute_single_cell(leading_cells[-1]):
                permute_cells_recursive(E_calc, leading_cells[0:-1], [permutation_of_cell] + trailing_cells )



    E_calculator = energy_calc.Energy_levels(fixed_particles, dielectric)

    start_time = time.time()

    permute_cells_recursive(E_calculator, cells, [])

    end_time = time.time()

    print("\n","Electron combinations, keeping electrons in cell")
    print("Time elapsed:", end_time - start_time, "count", E_calculator.count)

    return E_calculator.all_energies, E_calculator.all_configurations


def flip_only_electron_combinations(cells, fixed_particles, dielectric):
    """
    This function finds the lowest energy within qca cell, by only checkin two states (polarization +1 -1).
    Assumption: the electrons does not leave the cell they are in, and the lowest energy states are ones
    which have electrons at opposing corners

    :param cells: List[List[np.ndarray]]   --  cells -> particles_of_cell -> coodrinates_of_particle
    :param fixed_particles:  List[np.ndarray],  driver cell (e.g. two pos QD:s two neg QD:s)

    """
    def flip_cell(cell):
        """
        This function flips the cell that has FOUR quantum dots and TWO electrons
        """
        num_electrons = 0

        el_charge = cell[0][3]
        el_radius = cell[0][4]

        for prt in cell: # find electron-count in cell
            if prt[3]<-0.9: # negative charge
                num_electrons += 1
            else:
                break # electrons are first paricles in cell

        num_holes = len(cell) - 2*num_electrons
        els = cell[:num_electrons]
        qdots = cell[num_electrons:]

        #if ( (num_electrons != 2) or (num_holes != 2) or (len(qdots) != 4) ):
        #    raise Exception("This function works only for four QDs and two electrons!")

        #                           coordinates     electron-info
        electrons_flip = [np.array([*qdots[0][0:3], *els[0][3:]]), np.array([*qdots[2][0:3], *els[0][3:]])]
        electrons_flop = [np.array([*qdots[1][0:3], *els[0][3:]]), np.array([*qdots[3][0:3], *els[0][3:]])]

        states_of_cell = [electrons_flip + qdots, electrons_flop + qdots]

        return states_of_cell

    def permute_cells_recursive(E_calc, leading_cells, trailing_cells):

        if (len(trailing_cells) == 0):
            print("Permutation progress (/16) ",end="")
        elif (len(trailing_cells) == 4):
            print("#",end="")
            sys.stdout.flush()

        if (len(leading_cells) == 0):
            E_calc.store_energy(trailing_cells)
            return
        else:
            for permutation_of_cell in flip_cell(leading_cells[-1]):
                permute_cells_recursive(E_calc, leading_cells[0:-1], [permutation_of_cell] + trailing_cells )


    E_calculator = energy_calc.Energy_levels(fixed_particles, dielectric)

    start_time = time.time()

    permute_cells_recursive(E_calculator, cells, [])

    end_time = time.time()

    print("\n","new_electron_combinations_flip_only")
    print("Time elapsed:", end_time - start_time, "count", E_calculator.count,"\n")

    return E_calculator.all_energies, E_calculator.all_configurations



