#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

import os
import sys
# Python is broken by default. This is required to import modules when executing from path
# sys.path.insert(0,os.path.dirname(os.path.realpath(__file__)))

from . import plotting  # result_analysis.boltz_E.plotting
from . import electron_combinations
from . import read_paths
from . import miscellaneous  # result_analysis.boltz_E.miscellaneous
from . import energy_calc
from . import polarization_calcs


help_text = """
This is a updated version of classical energies. This script calculates (AND PLOTS) the lowest energy
state by permuting through different electron location combinations and finding the lowest energy state
in quantumdot arrangement.

Main idea of this program: There are positive quantum dots in QCA-cells, place electrons inside them
in different permutations, and find lowest energy.

This script can be used directly to plot stuff or indirectly as a module for all its functions.

Usage (preparation):
    # first generate qca arrangement to 'paths/' with 'qca_tiles' (or qca_2d_cell_gen.py) using GRID_CONF
    $ qca_tiles

    # (If not generated this way, there does not exist paths/002_init, so it is assumed that there are
    # two electrons per one cell.

    # This script is run inside the calculation directory, where data is read from QD, QCA, paths/, particles, CONF ...
    $ python3 -m classical_energies_2 [optional arguments]

arguments
    --path asdf/ghjk    path for calulation directory (default: current working directory)

    --check
            permute_all     checks all possible states (recommended <= 3 cells)
            permute_cell    fixes 2 electrons in cell (recommended <= 6 cells)
            flip_only       (default) flips only two states of cell (recommended <= 15 cells)

    --fix 110101    (example). Binary string for '1' to fix that cell, and not permute it. Order
                    of cells in x-y-plot is [up-down(left-rigth)], see QCA for exact oredring numbers.

    --exact-E       uses iterative method to find better energy-minimums for eletrons in quantum dots.
                    (Warning: this can be time consuming when more than 7 cells)

    --not_verbose   by default some progress information is printed, which is usefull when progress takes long.

    --plot 25       number of configurations to be plotted, (default 9, max 30)

    --help          print this poor guide

example
    python3 -m classical_energies_2 --check permute_cell

"""


TODO = """
Voi laskea solujonon PIMC-polarisaatiot, joita voi verrata klassisiin arvoihin.

Käyttö:
    cd varioi_parametria_hakemisto
    octave lue_arvot_ja_tallenna.m
    python3 ṕlottaa_ja_tallenna_molemmat_polarisaatiot

Todo: pythoniin pakkotyyppitarkastus! Perkele! Siis virhe jos ei tyyppiä
"""


if (sys.version_info.major < 3) or (sys.version_info.minor < 6):
    raise Exception("You must be running python 3.6 or newer for typing-feature")


def main(argv):
    

    path, check_states, to_be_fixed, exact_energies, plot_num = miscellaneous.parse_parametes(
                help_text, argv)


    cells, fixed_particles = read_paths.new_read_paths(path)
    dielectric, temperature = read_paths.read_dielectric_and_T_form_conf(path)


    # fixing cells means that they are locked in initial state, and electrons are not permuted over.
    if to_be_fixed is not None:
        cells, fixed_particles = miscellaneous.add_particles_to_fixed(to_be_fixed, cells, fixed_particles)

    
    all_energies, all_configurations = electron_combinations.choose_checked_states(
                cells, fixed_particles, dielectric, check_states)
    
    if exact_energies:
        all_energies, all_configurations, fixed_particles = energy_calc.correct_energies(\
                                all_energies, all_configurations, fixed_particles, dielectric)


    
    conf_pols = polarization_calcs.cell_polarizations(all_configurations)
    
    energies, confs = energy_calc.sort_by_energies(all_energies, all_configurations)
    energies, pols = energy_calc.sort_by_energies(all_energies, conf_pols)
    
    probs, thermal_pols = polarization_calcs.thermodynamical_mean_pols(energies, pols, temperature)    
    
    print("thermal_pols ")
    print("     ", thermal_pols)
    
    

    plotting.plot_energy_level_histogram(energies, temperature)


def clas_stat_polarizations(path="", check_states="permute_cell", to_be_fixed=None, 
                            exact_E=False, not_verbose=False):  # -> dict:
    """
    Classical statistical polarization information of qca configuration over all 
    possible electron states.
    
    Args:
        path: str
        check_states: str           "permute_all", "permute_cell", "flip_only", "choose-best"
        to_be_fixed: List[bool]     ie.: [1,1,0,0] does not permute two first cells
        exact_E: bool               increase accuracy by fiddling electrons to place
        not_verbose: bool           also known as 'quiet'. I could change better name for it,
                                    but this is joke about crappy variable naming
    
    Return:
        thermal_pols    thermal polarization means over all configurations 
                            (list: pol-per-cell)
        probs           probabilities of all configurations 
                            (list: probability per configuration)
        energies        energies of all configurations 
                            (list: energy per configuration)
        pols:           polarizations of all configurations per cell 
                            (list: conf[ list cells]   .. or something, read functions)
    """
    cells, fixed_particles = read_paths.new_read_paths(path)
    dielectric, temperature = read_paths.read_dielectric_and_T_form_conf(path)


    # fixing cells means that they are locked in initial state, and electrons are not permuted over.
    if to_be_fixed is not None:
        cells, fixed_particles = miscellaneous.add_particles_to_fixed(to_be_fixed, cells, fixed_particles)

    # quiet-mode, no output, lazy fix
    if not_verbose:
        old_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')
    all_energies, all_configurations = electron_combinations.choose_checked_states(
                cells, fixed_particles, dielectric, check_states)
    
    if exact_E:
        all_energies, all_configurations, fixed_particles = energy_calc.correct_energies(
                                all_energies, all_configurations, fixed_particles, dielectric)
    if not_verbose:
        sys.stdout = old_stdout

    
    conf_pols = polarization_calcs.cell_polarizations(all_configurations)
    
    energies, confs = energy_calc.sort_by_energies(all_energies, all_configurations)
    energies, pols = energy_calc.sort_by_energies(all_energies, conf_pols)
    
    probs, thermal_pols = polarization_calcs.thermodynamical_mean_pols(energies, pols, temperature)
    
    # this return is dict, used in "classical system data()", "mat"
    return {"thermal_pols":thermal_pols, "probs":probs, "energies":energies, 
            "pols":pols, "temperature":temperature, "dielectric":dielectric,
            "fixed_particles":fixed_particles, "confs_128":confs[0:(min(len(confs), 128))]}

if __name__ == "__main__":
    main(sys.argv[1:])
