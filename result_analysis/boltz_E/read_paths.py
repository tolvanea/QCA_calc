#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

import sys
import os #path.exists, path.isfile, getcwd
from typing import Tuple, List
import math
import numpy as np

import miscellaneous.read_conf as read_conf




def new_read_paths(path):
    """
    Read loactions of electrons and other particles from 'paths/002'. This function
    is meant mainly for combination calculations. QCA-cells and driver is separated
    to [cells] and [fixed_particles], using info from QCA and QD files.
    Hindsight: I should have used classes for cells and particles, instead of lists
    and np.ndarrays, it would have made code more intuitive to read and adapt.

    zeroth part: init things that will be used
    First part:  read particles to lists electrons, pos_qdots neg_qdots
    Second part: arrange particles to qca-cells and driver
    Third part:  order quantum dots to order [a,b;d,c]

    Cell structure : [*electrons, *pos_QDs], preferred: [el,el,ps,ps,ps,ps]
    """
    if (path != "" and path[-1] != "/"):
        path_slash = path+"/";
    else:
        path_slash = path
    
    FLAG_init_paths=False
    if os.path.exists(path_slash+"paths/002_initial"):
        FLAG_init_paths=True
        path001 = path_slash+"paths/002_initial"
    else:
        print("No init paths found, trickery may be used in calulations")
        if not os.path.exists(path_slash+"paths/001"):
            raise Exception("No paths/001 found, are you in correct directory?\n        searched in --> "+path);
        else:
            path001 = path_slash+"paths/001"


    def read_QD_side_length(path_slash) -> float:
        side_length: float = 0.0
        with open(path_slash+"QD", "r") as file:
            lines = file.readlines();
            # Assuming square cells
            side_length1 = abs(float(lines[0].split()[0]) - float(lines[1].split()[0]))
            side_length2 = abs(float(lines[0].split()[0]) - float(lines[2].split()[0]))
            side_length = max(side_length1, side_length2)
        return side_length


    def read_QCA_cell_locations(path_slash) -> List[Tuple[float, float, float]]:
        with open(path_slash+"QCA", "r") as file:
            lines = file.readlines();
            qc_coords=[]
            for line in lines:
                parts = line.split()
                qc_coords.append((float(parts[2]), float(parts[3]), float(parts[4])))
        return qc_coords


    # Information about particles:

    soften_radius = float(read_conf.read_config_line_pair(path_slash+"CONF","SoftenRadius"))
    charge_el = float(read_conf.read_config_line_values(path_slash+"PARTICLES","el")[1])
    charge_ps = float(read_conf.read_config_line_values(path_slash+"PARTICLES","ps")[1])
    # Read QD
    cell_side_length = read_QD_side_length(path_slash)
    # Read QCA
    QC_locations = read_QCA_cell_locations(path_slash)




    ### First part:  read particles to lists electrons, pos_qdots neg_qdots


    # arrays of all electrons and quantum dots to be returned
    electrons=[]
    pos_qdots=[]
    neg_qdots=[]
    fixed_qdots=[]
    #all_particles = [];

    path_file_names = []
    for f in os.listdir(path001):
         # all filenames in argument-path that ends in ".path"
        if (os.path.isfile(path001+"/"+f) and len(f)>5 and (f[-5:])==".path"):
            path_file_names.append(f)

    for file_name in path_file_names:
        with open(path001+"/"+file_name, "r") as file:
            line = file.readline();
            parts = line.split()
            
            if (file_name[0:2] == "el"):
                #charge = -1.0;
                charge = charge_el
                radius = 0.0;
                electrons.append(np.array([float(parts[0]),float(parts[1]),float(parts[2]),float(charge),float(radius)]));
                
            elif(file_name[0:2] == "ps"):
                #charge = 0.5;
                charge = charge_ps
                radius = soften_radius;
                pos_qdots.append(np.array([float(parts[0]),float(parts[1]),float(parts[2]),float(charge),float(radius)]));
                
            elif(file_name[0:2] == "mn"):
                #charge = -0.5;
                charge_mn = float(read_conf.read_config_line_values(path_slash+"PARTICLES","mn")[1])
                charge = charge_mn
                radius = soften_radius;
                neg_qdots.append(np.array([float(parts[0]),float(parts[1]),float(parts[2]),float(charge),float(radius)]));
                
            # charges used, for example in AND and OR testing
            elif(file_name[0:2] in ["pd", "p1", "p2", "p3", "m1", "m2", "m3"]):
                # this is a custom type to 
                #charge = 0.5;
                charge = float(read_conf.read_config_line_values(path_slash+"PARTICLES", file_name[0:2])[1])
                radius = soften_radius;
                fixed_qdots.append(np.array([float(parts[0]),float(parts[1]),float(parts[2]),float(charge),float(radius)]));
                
            else:
                raise Exception("Unknown particle " + file_name[0:2] + ". If it's quantum dot, then modify the code like in 'pd' -case,\n and understand that things may not work as excepted.");
                # Assuming quantum dot with radius
                charge = charge_ps
                radius = soften_radius;
                fixed_qdots.append(np.array([float(parts[0]),float(parts[1]),float(parts[2]),float(charge),float(radius)]));



    ### Second part: arrange particles to qca-cells and driver


    # new arrangement      0     1     2      3      4
    # particle np.array [crd1, crd2, crd3, charge, radius]

    #electrons_cpy = electrons[:] # I didn't know that this is way to copy python list
    #pos_qdots_cpy = pos_qdots[:] # UPDATE: It is still not copying, slicing numpy array is a reference


    cells : List[List[np.ndarray]] = []
    # iterate through cell locations
    for loc in QC_locations:
        cell : List[np.ndarray] = []


        # add electrons to cell
        if FLAG_init_paths:
            for i in range(len(electrons)-1,-1,-1):
                el=electrons[i]
                if ((abs(loc[0]-el[0]) < cell_side_length/1.9) \
                        and (abs(loc[1]-el[1]) < cell_side_length/1.9)):
                    el = electrons.pop(i)
                    cell.append(el)
        else: # add electrons to opposing corners when their place is not defined in paths/002_initial
            x = loc[0] - cell_side_length/2
            y = loc[1] - cell_side_length/2
            el = np.array([x, y, 0.0, electrons[0][3], electrons[0][4]])
            cell.append(el)

            x = loc[0] + cell_side_length/2
            y = loc[1] + cell_side_length/2
            el = np.array([x, y, 0.0, electrons[0][3], electrons[0][4]])
            cell.append(el)

            electrons.pop()
            electrons.pop()

        # error checing, the rest code is designed so that there can be arbitary number of electrons and QDs
        if (len(cell) != 2):
            raise Exception("There's "+str(len(cell))+"!=2 electrons in cell, something is wrong")

        # Add quantum dots to cell
        for i in range(len(pos_qdots)-1,-1,-1):
            ps=pos_qdots[i]
            if ((abs(loc[0]-ps[0]) < cell_side_length/1.9) \
                    and (abs(loc[1]-ps[1]) < cell_side_length/1.9)):
                ps = pos_qdots.pop(i)
                cell.append(ps)

        # error checing
        if (len(cell) != 6):
            raise Exception("There's "+str(len(cell)-2)+"!=4 QD:s in cell, something is wrong")

        cells.append(cell)

    if (len(electrons) != 0):
        raise Exception("There's "+str(len(electrons))+" excess electrons")

    if not ((len(pos_qdots) == 0) or (len(pos_qdots) == 2)):
        input("There's "+str(len(pos_qdots))+" excess qd:s, continue? (Enter / ^C)")


    """ Third part: order quantum dots inside cell to [a,b:d,c]"""


    cells_ordered = []
    for cell in cells:
        qdots = cell[2:]    # strip electrons off
        qdots_ordered = [None for i in range(4)]

        max_x = max(qdots[0][0], qdots[1][0], qdots[2][0], qdots[3][0])
        min_x = min(qdots[0][0], qdots[1][0], qdots[2][0], qdots[3][0])
        max_y = max(qdots[0][1], qdots[1][1], qdots[2][1], qdots[3][1])
        min_y = min(qdots[0][1], qdots[1][1], qdots[2][1], qdots[3][1])

        center_x = (max_x+min_x)/2
        center_y = (max_y+min_y)/2

        xypair = [(-1,1), (1,1), (1,-1), (-1,-1)] # this is the preferred order for xy-coordinates
        # the following does the ordering
        for i in range(4):
            for j in range(4):
                if (qdots[i][0]-center_x)*xypair[j][0] > 0 and \
                        (qdots[i][1]-center_y)*xypair[j][1] > 0 :
                    qdots_ordered[j] = qdots[i]
                    break
                if j==3:
                    raise Exception("Noeh! Something wrong!")

        cells_ordered.append(cell[0:2]+qdots_ordered) # paste electrons


    # more testing
    for cell in cells_ordered:
        qdots = cell[2:]    # strip electrons off
        adj_dist_sqr = (qdots[0][0]-qdots[1][0])**2 + (qdots[0][1]-qdots[1][1])**2
        opposing_dist_sqr = (qdots[0][0]-qdots[2][0])**2 + (qdots[0][1]-qdots[2][1])**2
        if not math.isclose(adj_dist_sqr*2, opposing_dist_sqr, rel_tol=1e-5):
            input("Warning: Quantum dots are not in square, (though that does not matter), continue? (Enter / ^C)")



    # the driver cell
    fixed_particles : List[np.ndarray] = pos_qdots + neg_qdots + fixed_qdots

    return cells_ordered, fixed_particles


def read_dielectric_and_T_form_conf(path=""):

    if (path != "" and path[-1] != "/"):
        path_slash = path+"/";
    else:
        path_slash = path;

    eps_r = float(read_conf.read_config_line_pair(path_slash+"CONF","Dielectric"))
    T = float(read_conf.read_config_line_pair(path_slash+"CONF","Temperature"))


    return eps_r, T
