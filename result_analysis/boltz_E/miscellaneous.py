#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

from typing import Tuple, List
import numpy as np


def parse_parametes(help_text,argv):
    """
    Parse bash parameters
    """

    if (("--path" in argv) and (argv.index("--path")+1 < len(argv))):
        path = argv[argv.index("--path")+1]
    else:
        path = ""

    if (("--check" in argv) and (argv.index("--check")+1 < len(argv))):
        check_states = argv[argv.index("--check")+1]
    else:
        check_states = "flip_only"

    if (("--fix" in argv) and (argv.index("--fix")+1 < len(argv))):
        fix_string = argv[argv.index("--fix")+1]
        to_be_fixed : List[bool] = []
        for char in fix_string:
            if (char=="1"):
                to_be_fixed.append(True)
            elif (char=="0"):
                to_be_fixed.append(False)
            else:
                raise Exception("Malformed string "+fix_string+" use only 0 or 1")
    else:
        to_be_fixed=None

    if ("--exact-E" in argv):
        exact_energies = True
    else:
        exact_energies = False

    if ("--not_verbose" in argv):
        FLAG_print_stuff = False
    else:
        FLAG_print_stuff = True

    if (("--plot" in argv) and (argv.index("--plot")+1 < len(argv))):
        plot_num = int(argv[argv.index("--plot")+1])
    else:
        plot_num = 9

    if ("--help" in argv):
        print(help_text)
        exit(0)

    return path, check_states, to_be_fixed, exact_energies, plot_num




def add_particles_to_fixed(to_be_fixed, cells, fixed_particles):
    """
    Takes particles from cells and moves them to fixed particles. Is handy for reducing permutations.

    :param: to_be_fixed boolean list that corresponds the cells to be moved out.
    :param: cells, fixed_particles will not be modified

    returns: new copy of cells and fixed_particles
    """

    if (len(to_be_fixed) != len(cells)):
        raise Exception("Lengths of fixing cells does not match")

    fixed_particles_added : List[np.ndarray] = fixed_particles[:]
    cells_reduced : List[List[np.ndarray]] = cells[:]

    for i in range(len(cells)):
        if to_be_fixed[i]:
            cell = cells[i]
            cells_reduced.remove(cell)
            fixed_particles_added.extend(cell)

    return cells_reduced, fixed_particles_added






def poor_mans_test_unit__print_cells(cells):
    print("poor_mans_test_unit: print_cells")
    for cell in cells:
        print("cell")
        for prt in cell:
            print("    ", prt)
    print("\n")


def poor_mans_test_unit__print_particles(paricles):
    print("poor_mans_test_unit: print_particles")
    for prt in paricles:
        print("    ", prt[0:5])
    print("\n")

def poor_mans_test_unit__print_cell_conf(confs):
    print("poor_mans_test_unit: print_cells")
    for i, cells in enumerate(confs):
        print("conf",i)
        for j, cell in enumerate(cells):
            print("    cell",j)
            for k, prt in enumerate(cell):
                print("        prt:"+str(k)+" ", prt)
        print("\n")
    print("\n")
    
def poor_mans_test_unit__print_polarisations(energies,pols):
    print("poor_mans_test_unit: print_polarisations")
    for i in range(len(pols)):
        print("i",i, energies[i], pols[i])
