#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT

from typing import Tuple, List
import numpy as np
import matplotlib.pyplot as plt
import random
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection
import math
import os  # path.exists, path.isfile, getcwd

import miscellaneous.natural_constants_SI as nc


def get_lims(cells, fixed_particles):
    """
    Gets the limits for the plot, takes it from the particles farthest apart
    """
    max_x = -100
    max_y = -100
    min_x = 100
    min_y = 100

    for cell in cells:
        for prt in cell:
            max_x = max(max_x, prt[0])
            max_y = max(max_y, prt[1])
            min_x = min(min_x, prt[0])
            min_y = min(min_y, prt[1])


    for prt in fixed_particles:
        max_x = max(max_x, prt[0])
        max_y = max(max_y, prt[1])
        min_x = min(min_x, prt[0])
        min_y = min(min_y, prt[1])

    return min_x, max_x, min_y, max_y


def plot_permutation(cells, fixed_particles, linewidth=1, ax=None):

    # new arrangement      0     1     2      3      4
    # particle np.array [crd1, crd2, crd3, charge, radius]

    all_particles = []  # I dissaprove this copy-list-reference behaviour of python!
    for cell in cells:
        all_particles.extend(cell)
    
    # Driver QDs are now separate, and plotted as filled circles, if not change to True
    if False:  # False: uniformly plotted quantumdots
        all_particles.extend(fixed_particles[:])
        pos_qdots_driver = []
        neg_qdots_driver = []
    else:  # True: filled driver dots
        pos_qdots_driver = []
        neg_qdots_driver = []
        
        for prt in fixed_particles:
            if (-0.6 < prt[3] < -0.0):
                neg_qdots_driver.append(prt)
            elif (0.0 < prt[3] < 0.6):
                pos_qdots_driver.append(prt)
            elif (-0.001 < prt[3] < 0.001):
                pass
            else:
                raise Exception("Unknown particle in plotting, properties [crd1, crd2, crd3, charge, radius] = ", 
                                prt, ". Notice that charges are expected to be on range -0.5e - +0.5e." +
                                " If not, modify the code here.")

    electrons=[]
    pos_qdots=[]
    neg_qdots=[]    # this really should be empty, as there is no negative QDs in QCA cell structure
    for prt in all_particles:
        if (-1.1 < prt[3] < -0.9) and (prt[4] < 0.001):
            electrons.append(prt)
        elif (-0.6 < prt[3] < -0.0):
            neg_qdots.append(prt)
        elif (0.0 < prt[3] < 0.6):
            pos_qdots.append(prt)
        elif (-0.01 < prt[3] < 0.01):
            pass
        else:
            raise Exception("Unknown particle in plotting, properties [crd1, crd2, crd3, charge, radius] = ", 
                            prt, ". Notice that charges are expected to be on range -1e - +1e. If not, modify this code.")
    x_pos_qdots = [p_qdot[0] for p_qdot in pos_qdots]
    y_pos_qdots = [p_qdot[1] for p_qdot in pos_qdots]
    x_pos_qdots_driver = [p_qdot[0] for p_qdot in pos_qdots_driver]
    y_pos_qdots_driver = [p_qdot[1] for p_qdot in pos_qdots_driver]
    # radius_pos = pos_qdots[0][4]
    # radiai_pos = [radius_pos for i in range(len(pos_qdots))]
    radiai_pos = [pQD[4] for pQD in pos_qdots]
    radiai_pos_driver = [pQD[4] for pQD in pos_qdots_driver]

    
    x_neg_qdots = [n_qdot[0] for n_qdot in neg_qdots]
    y_neg_qdots = [n_qdot[1] for n_qdot in neg_qdots]
    x_neg_qdots_driver = [n_qdot[0] for n_qdot in neg_qdots_driver]
    y_neg_qdots_driver = [n_qdot[1] for n_qdot in neg_qdots_driver]
    radiai_neg = [nQD[4] for nQD in neg_qdots]
    radiai_neg_driver = [nQD[4] for nQD in neg_qdots_driver]


    x_els = [el[0] for el in electrons]
    y_els = [el[1] for el in electrons]
    radiai_el = [0.1 for i in range(len(electrons))]

    out = circles(x_pos_qdots, y_pos_qdots, radiai_pos, ax=ax, c='r', fc='none', lw=linewidth)
    out = circles(x_neg_qdots, y_neg_qdots, radiai_neg, ax=ax, c='b', fc='none', lw=linewidth)
    out = circles(x_pos_qdots_driver, y_pos_qdots_driver, radiai_pos_driver, ax=ax, c='r', fc='r', lw=linewidth)
    out = circles(x_neg_qdots_driver, y_neg_qdots_driver, radiai_neg_driver, ax=ax, c='b', fc='b', lw=linewidth)
    out = circles(x_els, y_els, radiai_el, ax=ax, color=(0,0,1))

    if ax is None:
        plt.axis('equal')
    else:
        ax.axis('equal')

    min_x, max_x, min_y, max_y = get_lims(cells,fixed_particles)
    if (max_y-min_y) < 12:
        if ax is None:
            plt.axis([min_x, max_x, -7, 7])
        else:
            ax.axis([min_x, max_x, -7, 7])


def plot_permutations(top_30_E, top_30_cells_conf, fixed_particles, title : str, 
                      plot_num = 9, num_subplots=3, save_name=None, show=False):

    num_subplots = num_subplots
    plot_num = min(30, plot_num) # 30 is max
    num_figs = math.ceil(plot_num/num_subplots)

    # crop out empty ones, if there are those.
    for itr in range(len(top_30_cells_conf)):
        if top_30_cells_conf[itr] is None:
            if (num_figs >= itr):
                num_figs = math.ceil((itr)/num_subplots)
            break

    for i0 in range(num_figs):
        #plt.figure(random.randint(1,1000))
        f, axarr = plt.subplots(num_subplots, 1, figsize=(6, 12))
        for i in range(num_subplots):  #len(top_30_cells_conf)):
            if (i+num_subplots*i0 < len(top_30_cells_conf)) and (top_30_cells_conf[i+num_subplots*i0] is not None):
                plt.sca(axarr[i])
                plot_permutation(top_30_cells_conf[i+num_subplots*i0], fixed_particles)
                plt.title(str(i+1+num_subplots*i0)+". " + str(round(top_30_E[i+num_subplots*i0]-top_30_E[0],6))+" eV")
                plt.xlabel("x (nm)")
                plt.ylabel("y (nm)")
            else:
                break

        #f.subplots_adjust(hspace=0.5)
        f.tight_layout()
        f.suptitle(title+", ground state = "+str(round(top_30_E[0],6))+" eV", fontsize=13)
        #plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
        
        save_name = save_name or os.getcwd().split(os.sep)[-1]+"_energies"+str(i0+1)+'.png'
        plt.savefig(save_name)
    
    if show:
        plt.show()


#https://stackoverflow.com/questions/9081553/python-scatter-plot-size-and-style-of-the-marker/24567352#24567352
def circles(x, y, s, c='b', ax=None, vmin=None, vmax=None, **kwargs):
    """
    Make a scatter of circles plot of x vs y, where x and y are sequence
    like objects of the same lengths. The size of circles are in data scale.

    Parameters
    ----------
    x,y : scalar or array_like, shape (n, )
        Input data
    s : scalar or array_like, shape (n, )
        Radius of circle in data unit.
    c : color or sequence of color, optional, default : 'b'
        `c` can be a single color format string, or a sequence of color
        specifications of length `N`, or a sequence of `N` numbers to be
        mapped to colors using the `cmap` and `norm` specified via kwargs.
        Note that `c` should not be a single numeric RGB or RGBA sequence
        because that is indistinguishable from an array of values
        to be colormapped. (If you insist, use `color` instead.)
        `c` can be a 2-D array in which the rows are RGB or RGBA, however.
    vmin, vmax : scalar, optional, default: None
        `vmin` and `vmax` are used in conjunction with `norm` to normalize
        luminance data.  If either are `None`, the min and max of the
        color array is used.
    kwargs : `~matplotlib.collections.Collection` properties
        Eg. alpha, edgecolor(ec), facecolor(fc), linewidth(lw), linestyle(ls),
        norm, cmap, transform, etc.

    Returns
    -------
    paths : `~matplotlib.collections.PathCollection`

    Examples
    --------
    a = np.arange(11)
    circles(a, a, a*0.2, c=a, alpha=0.5, edgecolor='none')
    plt.colorbar()

    License
    --------
    This code is under [The BSD 3-Clause License]
    (http://opensource.org/licenses/BSD-3-Clause)
    """
    if np.isscalar(c):
        kwargs.setdefault('color', c)
        c = None
    if 'fc' in kwargs: kwargs.setdefault('facecolor', kwargs.pop('fc'))
    if 'ec' in kwargs: kwargs.setdefault('edgecolor', kwargs.pop('ec'))
    if 'ls' in kwargs: kwargs.setdefault('linestyle', kwargs.pop('ls'))
    if 'lw' in kwargs: kwargs.setdefault('linewidth', kwargs.pop('lw'))

    patches = [Circle((x_, y_), s_) for x_, y_, s_ in np.broadcast(x, y, s)]
    collection = PatchCollection(patches, **kwargs)
    if c is not None:
        collection.set_array(np.asarray(c))
        collection.set_clim(vmin, vmax)

    ax_old = plt.gca()
    if ax is None:
        ax = ax_old
    else:
        plt.sca(ax)
        
    ax.add_collection(collection)
    ax.autoscale_view()
    if c is not None:
        plt.sci(collection)
    
    plt.sca(ax_old)
    
    return collection


def print_permutations(top_30_E):
    print("Top ten energies, lowest:",round(top_30_E[0],10))
    for i, E in enumerate(top_30_E):
        print(i," ",round(E-top_30_E[0],10))
        if i >= 9:
            break

def plot_energy_level_histogram(energies, temperature):
        
    fig, (ax1) = plt.subplots(1, 1, num='histogram')
    x_axis = np.array(list(range(len(energies))))
    
    kT = temperature * nc.k_B / nc.eV
    
    plt.bar(x_axis+0.5, energies - energies[0], 0.90)
    plt.plot([x_axis[0], x_axis[-1]+1], [kT]*2, color="C2")
    #plt.plot([x_axis[0], x_axis[-1]], [- energies[0]]*2)
    ax1.text(-0.1, kT, "$k_BT$", color="C2", verticalalignment='center', 
             horizontalalignment='right', fontsize=12)
    
    plt.xlim([x_axis[0], x_axis[-1]+1])
    plt.xlabel("konfiguraatio nro.")
    plt.ylabel("$E$ (eV)")
    #plt.ylim(ylim)
    plt.show()
    
    #block_print()
    #tikz_save("figures/" + savename + '.tikz')
    #enable_print()
    

