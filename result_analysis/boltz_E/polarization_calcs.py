#!/usr/bin/env python3.6

# Alpi Tolvanen 2017, Licence MIT
from typing import List, Tuple
import numpy as np

from . import miscellaneous  # result_analysis.boltz_E.miscellaneous
import miscellaneous.natural_constants_SI as nc  # QCA_calc/miscellaneous


def cell_polarizations(cell_confs) -> List[int]:
    """
    Polarizations (1,-1,0,1...) from configuration
    """
    
    #miscellaneous.poor_mans_test_unit__print_cell_conf(cell_confs)
    
    confs = [None for i in range(len(cell_confs))]
    for ind0, cells in enumerate(cell_confs):
        
        pols = [-2 for i in range(len(cells))]
        for ind1, cell in enumerate(cells):
            els = cell[0:2]
            qdots = cell[2:6]    # strip electrons off
            
            #miscellaneous.poor_mans_test_unit__print_cells([cell])
            max_x = max(qdots[0][0], qdots[1][0], qdots[2][0], qdots[3][0])
            min_x = min(qdots[0][0], qdots[1][0], qdots[2][0], qdots[3][0])
            max_y = max(qdots[0][1], qdots[1][1], qdots[2][1], qdots[3][1])
            min_y = min(qdots[0][1], qdots[1][1], qdots[2][1], qdots[3][1])

            center_x = (max_x+min_x)/2
            center_y = (max_y+min_y)/2
            
            #a = (qdots[0][0] - center_x, qdots[0][1] - center_y, qdots[0][2])
            #b = (qdots[1][0] - center_x, qdots[1][1] - center_y, qdots[1][2])
            #c = (qdots[2][0] - center_x, qdots[2][1] - center_y, qdots[2][2])
            #d = (qdots[3][0] - center_x, qdots[3][1] - center_y, qdots[3][2])
            
            e1 = (els[0][0] - center_x, els[0][1] - center_y, els[0][2])
            e2 = (els[1][0] - center_x, els[1][1] - center_y, els[1][2])
            
            if (e1[0] * e2[0] < 0) and (e1[1] * e2[1] < 0):
                if e1[0] * e1[1] < 0:
                    pol = -1
                else:
                    pol = 1
            else:
                pol = 0
            
            pols[ind1] = pol
        confs[ind0] = pols
    return confs


def thermodynamical_mean_pols(energies : List[float], pols : List[List[int]], T : int) \
        -> Tuple[List[float], List[float]]:
    """
    Caluculates the thermodynamical mean of polarisations
    
    Args:
        energies:    List of energies of diffenrent configurations
        pols:        List of lists of corresponding polarisation
        T:           Temperature of system (K)
    
    Return:
        conf_P:     List of cellarray with thermodynamical mean polarisations
    """
    
    # temperature in natural units
    temp = T * nc.conv_SI.T
    
    # energy with arbitary zero level (for numerical precision)
    E = (np.array(energies) - energies[0]) * nc.eV
    
    # Normalisation faction, partition function, total_probability
    tot_P = np.sum(np.exp(-E / (nc.k_B * temp)))
    #print(np.array(energies) )
    
    conf_P = np.full(len(energies), fill_value=0.0)
    conf_pol = np.full(len(pols[0]), fill_value=0.0)
    
    for i in range(len(E)):
        # probability of energy state
        P = np.exp(-E[i] / (nc.k_B * temp)) / tot_P
        
        # probability for configuration
        conf_P[i] = P
        
        # polarisations of configuration i
        pol = np.array(pols[i])
        
        conf_pol += pol * P
    
    return conf_P, conf_pol
    
        
        
