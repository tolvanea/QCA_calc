import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import matplotlib.ticker as mtick

from . import plot_dens
import miscellaneous.natural_constants_SI as nc


def q2p(q):
        """
        Polarisation from known charge of QD:s
        
        Args:
            q   charge in QD:s 1 or 3, (charge in QD:s 2,4 is -q)
        """
        electron_density_1_3 = 0.5 - q
        electron_density_2_4 = 0.5 + q
        pol = (electron_density_1_3 * 2 - electron_density_2_4 * 2) / \
            2*(electron_density_1_3 + electron_density_2_4)
        
        # This minus sign makes it so that when electrons are in cells 1,3 polarization is -1
        return -pol


def errorbar_xy(ax1, p_in, p_out, p_out_err, xl="", yl="", **kwargs):
    
    ax1.errorbar(p_in, p_out, p_out_err, capsize=3, **kwargs)
    
    if xl != "":
        ax1.set_xlabel(xl)
    if yl != "":    
        ax1.set_ylabel(yl)
    if "label" in kwargs.keys():
        ax1.legend()


def plot_comparision_line(ax1, p_in, raising):
    if raising:  # normal cell couling
        reference_linear_line = [-1,1]
    else:  # NOT
        reference_linear_line = [1,-1]        
    ax1.plot([p_in[0], p_in[-1]], reference_linear_line, 
             color="gray", linestyle="-", linewidth=0.5, zorder=-10)  # linestyle=(0, (5, 10))


# former plot_one_density
def plot_one_density_and_QDs(ax, init_params, clas_data, idx1=-1, idx2=-1, title=False):
    """
    Plot data loaded data from directories
    """
    
    confs_128 = clas_data.get_single_data("confs_128", idx1, idx2)
    cells = confs_128[0]  # classical electrons are removed for plot, so this idx does not matter
    fixed_particles = clas_data.get_single_data("fixed_particles", idx1, idx2)
    
    path = os.getcwd() + '/' + init_params.dirlabels1[idx1] + '/' + init_params.dirlabels2[idx2]
    
    plot_dens.plot_el_density_and_QD_circles(ax, cells, fixed_particles, path)
    
    if title:
        ax.set_title("Electron density of last cell " + 
                     init_params.dirlabels1[idx1] + '/' + init_params.dirlabels2[idx2])


def plot_histogram(ax3, clas_data, dirlabels12, idx1=-1, idx2=-1, title=False, zoomlens=False, annotations=False):
    # plot histogram of energy levels
    energies = clas_data.get_single_data("energies", idx1, idx2)
    
    temperature = clas_data.get_single_data("temperature", idx1, idx2)
    
    # prevent zoomlens in not-well-fitted situations
    if (len(energies) <= 20):
        zoomlens = False
        
    if "energies_flip" not in clas_data.cl_data[0][0]:
        plot_energy_level_histogram(ax3, energies, temperature, zoomlens=zoomlens, annotations=annotations)
    else:
        energies_flip = clas_data.get_single_data("energies_flip", idx1, idx2)
        plot_energy_level_histogram(ax3, energies, temperature, zoomlens=zoomlens, 
                                    annotations=annotations, energies_flip=energies_flip)
    if title:
        ax3.set_title(dirlabels12[0][idx1] + "/" + dirlabels12[1][idx2])
    


def plot_energy_level_histogram(ax1, energies, temperature, zoomlens=False, annotations=False, energies_flip=None):
    """
    zoomlens    - for wire and not
    annotations - for AND and OR
    """
        
    x_axis = np.array(list(range(len(energies))))
    
    kT = temperature * nc.k_B / nc.eV
    
    ax1.bar(x_axis, energies - energies[0], 0.70)
    ax1.plot([x_axis[0]-0.5, x_axis[-1]+0.5], [kT]*2, color="C2")
    #plt.plot([x_axis[0], x_axis[-1]], [- energies[0]]*2)
    ax1.text(-0.6, kT, "$k_BT$", color="C2", verticalalignment='center', 
             horizontalalignment='right', fontsize=12)  # k_BT_{300K}
    
    if energies_flip is not None:  # if both energies are calculated is same histogram
        histogram_flip_energies_chewing_gum_hack(ax1, x_axis, 
                                                 energies - energies[0], energies_flip - energies[0], 
                                                 bar_width=0.70)
    
    xlims = (x_axis[0]-0.5, x_axis[-1]+0.5)
    ylims = None
    
    if len(x_axis) < 20 :
        ylims = (0, 0.42)
    elif (20 <= len(x_axis) < 100): # Normal case multi-cell
        ylims = (0, 0.5)
    else: # multi cell, all states, (not only flip)
        xlims = (x_axis[0]-0.5, 119.5)
        ylims = (0, 0.42)
        
    if xlims is not None:
        ax1.set_xlim(xlims)
    if ylims is not None:
        ax1.set_ylim(ylims)
    ax1.set_xlabel("tila")
    ax1.set_ylabel("$E$ (eV)", size=12)
        
    if zoomlens :
        # zoom-factor: 2.4, location: upper-left
        ax_zoom = zoomed_inset_axes(ax1, 2.4, loc=2, bbox_to_anchor=(0.1, 1.01), bbox_transform=ax1.transAxes)
        wf = 1.0 # wf = (120/64)  # widen_factor
        ax_zoom.bar(x_axis * wf, energies - energies[0], 0.70*wf)
        ax_zoom.plot([xlims[0] * wf, xlims[1] * wf], [kT]*2, color="C2")
        ax_zoom.text(-0.6*wf, kT, "$k_BT$", color="C2", verticalalignment='center', 
                     horizontalalignment='right', fontsize=10)  # k_BT_{300K}$
        x1, x2, y1, y2 = -0.5*wf, 4.5*wf, 0, 0.09 # specify the limits
        if energies_flip is not None:
            histogram_flip_energies_chewing_gum_hack(ax_zoom, x_axis * wf, 
                                                     energies - energies[0], energies_flip - energies[0], 
                                                     bar_width=0.70*wf)
        
        ax_zoom.set_xlim(x1, x2) # apply the x-limits
        ax_zoom.set_ylim(y1, y2) # apply the y-limits
        
        plt.sca(ax_zoom)    # I would consider this feature as a bug, 
                            # why on earth it's impossible use axes directly?!?
        #plt.yticks(visible=False)
        #ax_zoom.set_xticks([1, 2, 3, 4], minor=True)
        plt.xticks(visible=False)
        #for label in ax_zoom.get_xticklabels()[::2]:
        #    label.set_visible(False)
        #ax_zoom.set_tick_params(visible=False)
        plt.tick_params(axis='both', which='major', labelsize=10)
        
        mark_inset(ax1, ax_zoom, loc1=2, loc2=4, fc="none", ec="0.5")
    
    # for side coupling
    if annotations:
        ax1.set_xlabel("")        
        ax1.text(-0.2, -kT*3, "perustila",         verticalalignment='center', horizontalalignment='center', fontsize=10)
        ax1.text(1, -kT*3,    "käännös-\nviritys", verticalalignment='center', horizontalalignment='center', fontsize=10)
        ax1.text(3.5, -kT*3,  "rinnakkaisviritykset", verticalalignment='center', horizontalalignment='center', fontsize=10)
        
        fmt = '$E_{x:.0f}$'
        tick = mtick.StrMethodFormatter(fmt)
        ax1.xaxis.set_major_formatter(tick) 
            
    # for corner coupling, white text for correct spacing
    if (not (annotations or zoomlens)) and (len(energies) < 10):
        ax1.set_xlabel("")
        ax1.text(-0.2, -kT*3, "perustila",           color="white", verticalalignment='center', horizontalalignment='center', fontsize=10)
        ax1.text(1, -kT*3, "käännös-\nviritys",      color="white", verticalalignment='center', horizontalalignment='center', fontsize=10)
        ax1.text(3.5, -kT*3, "rinnakkaisviritykset", color="white", verticalalignment='center', horizontalalignment='center', fontsize=10)
        
        fmt = '$E_{x:.0f}$'
        tick = mtick.StrMethodFormatter(fmt)
        ax1.xaxis.set_major_formatter(tick) 
        


def histogram_flip_energies_chewing_gum_hack(ax, x_axis, energies, energies_flip, bar_width=0.7):
    """
    Draw red flip-energies on top of normal ones
    """
    
    sparce_flipE = np.zeros(len(energies))
    
    indices = get_flip_indices_in_all_permutations(energies, energies_flip)
    sparce_flipE[indices] = energies[indices]
    ax.bar(x_axis, sparce_flipE, bar_width, color="firebrick")

    
        
def get_flip_indices_in_all_permutations(energies, energies_flip, err_label=None): # -> np.ndarray(dtype=int)
    # Returns the indices of flip-energies corresponding in all permutations- energies. 
    # This is done by (errorprone?) algorithm with matching the same energies.
    # Args:
    #       energies        - all permutation energies  len = 6^N
    #       energies_flip   - flip permutation energies len = 2^N
    #       err_label       - if defined (a string), error checking will be made
    
    # ENERGIES MUST BE IN ORDERED
    if not all(energies[i] <= energies[i+1] for i in range(len(energies)-1)):
        raise Exception("Energies not sorted")
    if not all(energies_flip[i] <= energies_flip[i+1] for i in range(len(energies_flip)-1)):
        raise Exception("Energies_flip not sorted!")
    
    # energies with ground state E_0, they SHOULD have same ground state though
    # energies_E0 = energies - energies[0]
    # energies_flip_E0 = energies_flip - energies_flip[0]
    
    f_itr = 0
    f_len = len(energies_flip)
    f_inds = np.full(f_len, fill_value=99999)
    for i in range(len(energies)):
        if energies_flip[f_itr] <= energies[i]:
            f_inds[f_itr] = i
            f_itr += 1
            if f_itr == f_len:
                break
    if f_itr != f_len:
        raise Exception("All values are not found! {} out of {}".format(f_itr, f_len))
            
    # test wether algortihm works correctly
    if err_label is not None:
        # energies with ground state E_0
        
        # check if the difference smaller than about a promille in every energy
        E_diff = np.abs(energies[f_inds] - energies_flip)
        tolerance = np.abs(energies_flip) * 0.0001 + 0.001
        E_is_E_flip = (E_diff < tolerance)
        
        is_my_alg_working = all(E_is_E_flip)
        
        if not is_my_alg_working:
            print("Comparision between all permutatins and flip states is not correct in " + err_label +
                  ".\n(this is kinda a bit of chewing gum fix)")
            print("Here's more data about it")
            print("     {} states out of {} was identified incorrectly".format(
                        np.sum(~E_is_E_flip),  # unary operator ~ inverts the np.array bool list
                        len(E_is_E_flip)))
            print("     The indices were:")
            print("         in cell_permutations: {}".format(f_inds[~E_is_E_flip]))
            print("         in flip: {}".format(np.arange(len(energies_flip))[~E_is_E_flip]))
            print("     Energy difference being (in eV) {}".format(E_diff[~E_is_E_flip]))
            print("")
    return f_inds

def fix_label_order_with_errorbars(ax1, **kwargs):
    """
    This functin assumes that your plot is 50% errorbar and 50% normal plot. 
    Its reorders labels so that errorbar is before normal plot
    """
    
    handles,labels = ax1.get_legend_handles_labels()
    
    if len(handles)%2 != 0:
        raise Exception("This works only when number of errorbars == num of normal plots")
    midway = round(len(handles)/2)
    
    new_order_handles = []
    new_order_labels = []
    for i in range(midway):
        new_order_handles.append(handles[midway+i])
        new_order_handles.append(handles[i])
        new_order_labels.append(labels[midway+i])
        new_order_labels.append(labels[i])
    
    ax1.legend(new_order_handles, new_order_labels, **kwargs)
    


def main(argv):
    raise Exception("Not (anymore) runnable, use more updated funtions")

if __name__ == "__main__":
    main(sys.argv)
    


