import os
import sys
import numpy as np
import matplotlib.pyplot as plt


from . import plot_dens
from . import plotting_1
import result_analysis.boltz_E.main as boltz_E 
import result_analysis.read_calc_data.read_calc_data as read_calc_data



def main(argv=sys.argv):
    
    # |initial parameters of all calculations (aka. calculation set)
    # |           |pimc data of all calculations
    # |           |          |classical data of all calculations
    init_params, pimc_data, clas_data = get_all_data()
    
    # run the plotting
    AND_and_OR(init_params, pimc_data, clas_data)
    
    plt.show()
    
    
def get_all_data():
    # check current directory is correct
    read_calc_data.initial_error_checking()
    
    # read initial parameters of calculation set, that are stored when the set is generated
    init_params = read_calc_data.read_initial_parameters()
    
    # read pimc-data and generate corresponding matlab-matrices in /results_data -directory.
    read_calc_data.octave_read_to_mat(init_params.dirname1, init_params.dirname2, init_params.len1, init_params.len2)
    
    # read the data from matrix that is generated above.
    pimc_data = read_calc_data.read_pimc_data_from_mat()
    
    # calculate or read classical data
    clas_data = read_calc_data.read_classical_system_data(os.getcwd(), init_params.dirlabels12)
    
    return init_params, pimc_data, clas_data
    


def AND_and_OR(init_params, pimc_data, clas_data):
    """
    AND an OR gates, correlation heatmap and density
    """
    
    if len(init_params.dirlabels1) != len(init_params.dirlabels2):
        raise Exception("This is not correct function to call for this calculation set")
    
    set_name = os.path.basename(os.getcwd())
    # figure window 1 box
    fig1, ax1 = plt.subplots(1, 1, num='AND_and_OR_map_qntm_' + set_name, figsize=(5.5,4))  # =(4.4,3.2))
    fig2, ax2 = plt.subplots(1, 1, num='AND_and_OR_map_clsc_' + set_name, figsize=(5.5,4))  # =(4.4,3.2))
    fig3, ax3 = plt.subplots(1, 1, num='AND_and_OR_density_'  + set_name, figsize=(4.5,4))  # =(3.6,3.2))
    fig4, ax4 = plt.subplots(1, 1, num='AND_and_OR_curve_'    + set_name, figsize=(5,4))    # =(4,3.2))


    
    p_1 = [plotting_1.q2p(q) for q in init_params.range1]
    p_2 = [plotting_1.q2p(q) for q in init_params.range2]
    
    l1 = len(init_params.dirlabels1)
    l2 = len(init_params.dirlabels2)
    
    pol_map_qt = np.empty((l1, l2))
    pol_map_cl = np.empty((l1, l2))
    
    P = pimc_data.P
    P_err = pimc_data.P_err
    pol_map_qt = P[:,:,-1]
    pol_map_qt_err = P_err[:,:,-1]
    
    P_cl = clas_data.mean_pols
    
    pol_map_cl = P_cl[:,:,-1]
    
    # Fixing my mistakes...
    if p_1[0] > p_1[-1]:
        pol_map_qt = np.flipud(pol_map_qt)
        pol_map_cl = np.flipud(pol_map_cl)
    if p_2[0] > p_2[-1]:
        pol_map_qt = np.fliplr(pol_map_qt)
        pol_map_cl = np.fliplr(pol_map_cl)
                        
                        
    # figure 1, polarization heat map quantum, and
    # figure 2, polarization heat map classical
    plot_fig12(ax1, ax2, l1, l2, pol_map_qt, pol_map_cl, fig1, fig2)
    
    # figure 3 density
    plot_fig3(ax3, init_params, clas_data, set_name)
    
    # figure 4 diagonal values
    plot_fig4(ax4, pol_map_qt, pol_map_qt_err, pol_map_cl, init_params.range2)
    
    # figure 5 (THIS IS A HACK) diagonal values of both '3_AND' and '4_OR' in same picture
    plot_fig5(init_params, pimc_data, clas_data, set_name)
    
    fig1.tight_layout()
    fig2.tight_layout()
    fig3.tight_layout()
    fig4.tight_layout()
    
    fig1.savefig("results_data/" + set_name + "_AND_and_OR_pol_qntm" +'.pdf')
    fig2.savefig("results_data/" + set_name + "_AND_and_OR_pol_clsc" + '.pdf')
    fig3.savefig("results_data/" + set_name + "_AND_and_OR_density" + '.pdf')
    fig4.savefig("results_data/" + set_name + "_AND_and_OR_curve" + '.pdf')
    
    #fig1.savefig("results_data/" + set_name + "_AND_and_OR_pol_qntm" +'.pgf')
    #fig2.savefig("results_data/" + set_name + "_AND_and_OR_pol_clsc" + '.pgf')
    #fig3.savefig("results_data/" + set_name + "_AND_and_OR_density" + '.pgf')
    #fig4.savefig("results_data/" + set_name + "_AND_and_OR_curve" + '.pgf')


def plot_fig12(ax1, ax2, l1, l2, pol_map_qt, pol_map_cl, fig1, fig2):
    # figure1, polarizatiom heat map
    # figure 2, density
    lim1 = 1 + 1/(l1)
    lim2 = 1 + 1/(l2)
    lims = [-lim2, lim2, -lim1, lim1]
    # imshow shows the matrix flipped up-down
    cax1 = ax1.imshow(np.flipud(pol_map_qt), extent=lims, cmap='RdBu_r', interpolation='nearest') 
    cax2 = ax2.imshow(np.flipud(pol_map_cl), extent=lims, cmap='RdBu_r', interpolation='nearest')
    ax1.set_xlabel("$P_B$", size=12, rotation=0)
    ax1.set_ylabel("$P_A$", size=12, rotation=0)
    ax2.set_xlabel("$P_B$", size=12, rotation=0)
    ax2.set_ylabel("$P_A$", size=12, rotation=0)

    # plt.rcParams.update({'font.size': 11})  # change all font sizes

    
    cbar1 = fig1.colorbar(cax1)  #, orientation='horizontal')
    cbar2 = fig2.colorbar(cax2) 
    cbar1.set_label('$\\langle P_D \\rangle$', rotation=0, size=12)
    cbar2.set_label('$\\langle P_D \\rangle$', rotation=0, size=12)
    cbar1.set_clim(-1, 1)
    cbar2.set_clim(-1, 1)
    
    # divide map in four sections
    ax1.plot([-lim1, lim1], [0, 0], color='k')
    ax1.plot([0, 0], [-lim2, lim2], color='k')
    
    ax2.plot([-lim1, lim1], [0, 0], color='k')
    ax2.plot([0, 0], [-lim2, lim2], color='k')
        
    
        
def plot_fig3(ax3, init_params, clas_data, set_name):
    
    #plotting_1.plot_one_density(ax3, dirlabels12, idx1=-1, idx2=0)
    # P_A = +1, P_B = -1
    plotting_1.plot_one_density_and_QDs(ax3, init_params, clas_data, idx1=-1, idx2=0)
    
    # labels    
    if set_name == "3_AND":
        ax3.text(-2.5, 4.5, "$P_A$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax3.text(-7.0, 0, "$P_C$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax3.text(-4.5, 0, "$-1$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax3.text(-2.5, -4.5, "$P_B$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax3.text(2.5, 0, "$\\langle P_D \\rangle$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
    elif set_name == "4_OR":
        ax3.text(-4.5, 0, "$+1$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)

    
def plot_fig4(ax4, pol_map_qt, pol_map_qt_err, pol_map_cl, range2, label_prequel="", color_idx=0):
    P_qt_diag       = np.empty(pol_map_cl.shape[0])
    P_qt_err_diag   = np.empty(pol_map_cl.shape[0])
    P_cl_diag       = np.empty(pol_map_cl.shape[0])
    for i in range(pol_map_cl.shape[0]):
        P_qt_diag[i] = pol_map_qt[i,i]
        P_qt_err_diag[i] = pol_map_qt_err[i,i]
        P_cl_diag[i] = pol_map_cl[i,i]
    
    p_in = [plotting_1.q2p(q) for q in range2]
        
    plotting_1.errorbar_xy(ax4, p_in, P_qt_diag, P_qt_err_diag,
                                  label=label_prequel+"PIMC",
                                  linestyle="-", color="C"+str(color_idx))
    ax4.plot(p_in, P_cl_diag, \
                label=label_prequel+"miehitysmalli",
                linestyle=":", color="C"+str(color_idx))
    ax4.set_xlabel("$P_A = P_B$", size=12, rotation=0)
    ax4.set_ylabel("$\\langle P_D \\rangle$", size=12, rotation=0)
    ax4.set_xlim((-1,1))
    ax4.set_ylim((-1.005,1.005))
    ax4.legend()


def plot_fig5(init_params1, pimc_data1, clas_data1, set_name):
    """
    Compares two different curves in one plot.
    This function is made to work only with two specific filenames
    """
    fig5, ax5 = plt.subplots(1, 1, num='AND_and_OR_both_curves_' + set_name, figsize=(5,4))
    
    if (set_name == "3_AND"):
        label_prequel1 = "AND "
        label_prequel2 = "OR "
        cd_to   = "../4_OR"
        cd_back = "../3_AND"
        
    elif (set_name == "4_OR"):
        label_prequel1 = "OR "
        label_prequel2 = "AND "
        cd_to   = "../3_AND"
        cd_back = "../4_OR"
        
    else:
        # This function is made to work only with theese two files
        return
    
    plot_fig4(ax5, pimc_data1.P[:,:,-1], pimc_data1.P_err[:,:,-1], 
              clas_data1.mean_pols[:,:,-1], init_params1.range2, label_prequel=label_prequel1, color_idx=0)
    
    # change directories back and forth
    os.chdir(cd_to)
    init_params2, pimc_data2, clas_data2 = get_all_data()
    
    plot_fig4(ax5, pimc_data2.P[:,:,-1], pimc_data2.P_err[:,:,-1], 
              clas_data2.mean_pols[:,:,-1], init_params2.range2, label_prequel=label_prequel2, color_idx=1)
    
    
    # labels gets messed up
    plotting_1.fix_label_order_with_errorbars(ax5, loc=2)
    
    os.chdir(cd_back)
    
    fig5.tight_layout()
    fig5.savefig("results_data/" + set_name + "_AND_and_OR_both_curves" + '.pdf')
    #fig5.savefig("results_data/" + set_name + "_AND_and_OR_both_curves" + '.pgf')


if __name__ == "__main__":
    main(sys.argv)
