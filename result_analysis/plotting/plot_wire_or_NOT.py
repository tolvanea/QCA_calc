import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import math

from . import plot_dens
from . import plotting_1
import result_analysis.boltz_E.main as boltz_E 
import result_analysis.read_calc_data.read_calc_data as read_calc_data
import miscellaneous.numpy_array_to_latex_table as latex_format


"""
This script plots figures used in bachelor thesis. Specified for calulation sets: '5_wire' and '6_NOT'
Implementations from 'plotting_1' are used.
"""


def main(argv=sys.argv):
    
    # |initial parameters of all calculations (aka. calculation set)
    # |           |pimc data of all calculations
    # |           |          |classical data of all calculations
    init_params, pimc_data, clas_data = get_all_data()
    
    # run the plotting
    wire_and_NOT(init_params, pimc_data, clas_data)
    
    # print the table
    print_data_table(init_params, pimc_data, clas_data)
    
    plt.show()
    
    
def get_all_data():
    # check current directory is correct
    read_calc_data.initial_error_checking()
    
    # read initial parameters of calculation set, that are stored when the set is generated
    init_params = read_calc_data.read_initial_parameters()
    
    # read pimc-data and generate corresponding matlab-matrices in /results_data -directory.
    read_calc_data.octave_read_to_mat(init_params.dirname1, init_params.dirname2, init_params.len1, init_params.len2)
    
    # read the data from matrix that is generated above.
    pimc_data = read_calc_data.read_pimc_data_from_mat()
    
    # calculate or read classical data
    clas_data = read_calc_data.read_classical_system_data(os.getcwd(), init_params.dirlabels12, both_permutations=True)
    
    return init_params, pimc_data, clas_data
    

def wire_and_NOT(init_params, pimc_data, clas_data):
    """
    For plotting correct figures for wire (7 QD:s) and not (7 QD:s)
    """
    
    set_name = os.path.basename(os.getcwd())
    # figure window 1 box
    fig1, ax1 = plt.subplots(1, 1, num='wire_and_NOT_coupling_' + set_name, figsize=(5,4))
    fig2, ax2 = plt.subplots(1, 1, num='wire_and_NOT_density_' + set_name, figsize=(5,4))
    fig3, ax3 = plt.subplots(1, 1, num='wire_and_NOT_histogram_' + set_name, figsize=(5,4))
    
    # x-axis, polarisation
    p_in = [plotting_1.q2p(q) for q in init_params.range2]
    
    P = pimc_data.P
    P_err = pimc_data.P_err
    
    P_cl = clas_data.mean_pols
    
    # figure 1
    plot_fig1(ax1, p_in, P, P_err, init_params.dirlabels12, init_params.latex_syms12, init_params.range1, P_cl)
        
    # figure 2, electron density
    plot_fig2(ax2, init_params, clas_data, set_name, labels=True)
    
    # figure 3 histogram
    plotting_1.plot_histogram(ax3, clas_data, init_params.dirlabels12, idx1=-1, idx2=-1, zoomlens=True)
    
    # figure 4 combine wire and not in same figure (THIS IS A HACK)
    plot_fig4(init_params, pimc_data, clas_data, set_name)
    
    fig1.tight_layout()
    fig2.tight_layout()
    fig3.tight_layout()
    
    fig1.savefig("results_data/" + set_name + "_wire_and_NOT_coupling" +'.pdf')
    fig2.savefig("results_data/" + set_name + "_wire_and_NOT_density" + '.pdf')
    fig3.savefig("results_data/" + set_name + "_wire_and_NOT_histogram" + '.pdf')
    fig3.savefig("results_data/" + set_name + "_wire_and_NOT_histogram" + '.png') # apparently .pdf file is huge
    
    #fig1.savefig("results_data/" + set_name + "_wire_and_NOT_coupling" +'.pgf')
    #fig2.savefig("results_data/" + set_name + "_wire_and_NOT_density" + '.pgf')
    #fig3.savefig("results_data/" + set_name + "_wire_and_NOT_histogram" + '.pgf')


def plot_fig1(ax1, p_in, P, P_err, dirlabels12, latex_syms12, range1, P_cl, label_prequel="", color_idx=0):
    """
    Plot first figure
    """
    len1 = len(dirlabels12[0])
    for idx in range(len1):
                        
        plotting_1.errorbar_xy(ax1, p_in, P[idx,:,-1], P_err[idx,:,-1],
                                      label="{}PIMC".format(label_prequel),
                                      linestyle="-", color="C"+str(idx+color_idx*len1)) # last cell
        ax1.plot(p_in, P_cl[idx, :, -1], \
                    label="{}miehitysmalli".format(label_prequel),
                    linestyle=":", color="C"+str(idx+color_idx*len1))
        ax1.set_xlabel("$P_0$", rotation=0, size=12)
        ax1.set_ylabel("$\\langle P_1 \\rangle$", rotation=0, size=12)
        ax1.set_xlim((-1,1))
        ax1.set_ylim((-1.005,1.005))
        ax1.xaxis.label.set_size(12)
        ax1.yaxis.label.set_size(12)
        #ax1.legend()
        #ax1.set_title("")
    
    plotting_1.plot_comparision_line(ax1, p_in, (P_cl[0, 0, -1] < P_cl[0, -1, -1]))
    
    plotting_1.fix_label_order_with_errorbars(ax1)


def plot_fig2(ax2, init_params, clas_data, set_name, labels=False):
    """
    Plot second figure, electron density
    """
    # plotting_1.plot_one_density(ax2, dirlabels12, idx1=-1, idx2=-1) #TODO RM
    plotting_1.plot_one_density_and_QDs(ax2, init_params, clas_data, idx1=-1, idx2=-1)
    
    if labels:
        if set_name == "5_suora":
            ax2.text(-13.5, -3, "$P_0$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
            ax2.text(+13.5, -3, "$\\langle P_1 \\rangle$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        elif set_name == "6_NOT":
            ax2.text(-9, -2.5, "$P_0$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
            ax2.text(4.5, -2.5, "$\\langle P_1 \\rangle$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        
        
def plot_fig4(init_params1, pimc_data1, clas_data1, set_name):
    """
    Compares two different curves in one plot.
    This function is made to work only with two specific filenames
    """
    fig4, ax4 = plt.subplots(1, 1, num='wire_and_NOT_both_curves_' + set_name, figsize=(5,4))
    
    if (set_name == "5_suora"):
        label_prequel1 = "Johdin "
        label_prequel2 = "NOT "
        cd_to   = "../6_NOT"
        cd_back = "../5_suora"
        
    elif (set_name == "6_NOT"):
        label_prequel1 = "NOT "
        label_prequel2 = "Johdin "
        cd_to   = "../5_suora"
        cd_back = "../6_NOT"
        
    else:
        # This function is made to work only with theese two files
        return
    
    p_in = [plotting_1.q2p(q) for q in init_params1.range2]
    plot_fig1(ax4, p_in, pimc_data1.P, pimc_data1.P_err, 
              init_params1.dirlabels12, init_params1.latex_syms12, 
              init_params1.range1, clas_data1.mean_pols, 
              label_prequel=label_prequel1, color_idx=0)
    
    # change directories back and forth
    os.chdir(cd_to)
    init_params2, pimc_data2, clas_data2 = get_all_data()
    
    p_in = [plotting_1.q2p(q) for q in init_params2.range2]
    plot_fig1(ax4, p_in, pimc_data2.P, pimc_data2.P_err, 
              init_params2.dirlabels12, init_params2.latex_syms12, 
              init_params2.range1, clas_data2.mean_pols, 
              label_prequel=label_prequel2, color_idx=1)
    
    os.chdir(cd_back)
    
    # labels gets messed up
    plotting_1.fix_label_order_with_errorbars(ax4)
    
    fig4.tight_layout()
    fig4.savefig("results_data/" + set_name + "_wire_and_NOT_both_curves" + '.pdf')
    #fig4.savefig("results_data/" + set_name + "_wire_and_NOT_both_curves" + '.pgf')



def print_data_table(init_params, pimc_data, clas_data):
    pimc_data, clas_data
    
    p_in = plotting_1.q2p(np.array(init_params.range2))
    
    P = pimc_data.P
    P_err = pimc_data.P_err
    
    P_cl      = clas_data.mean_pols
    P_cl_flip = clas_data.get_mean_pols("thermal_pols_flip")
    
    #Ep_cl      = clas_data.mean_Ep
    #Ep_cl_flip = clas_data.calc_mean_energy("energies_flip")
    
    energies      = clas_data.get_all_data("energies")
    energies_flip = clas_data.get_all_data("energies_flip")
    
    probs = clas_data.get_all_data("probs")
    
    if (len(energies) != 1):
        print("What are you doing? This function is meant for flat sets with 1st dimension of size 1")
    
    # if previous case is still not obeyed, lets make then that is asked (print excess amount of tables)
    for dim0_itr in range(len(energies)):
        len2 = len(energies[dim0_itr])
        prob_fracs = np.empty(len2, dtype=float)
        for i in range(len(energies[dim0_itr])):
            #def calc_prob_fraction():
            inds = plotting_1.get_flip_indices_in_all_permutations(energies[dim0_itr,i], 
                                                                   energies_flip[dim0_itr,i],
                                                                   err_label=init_params.dirlabels2[i])
            prob_fracs[i] = np.sum(probs[dim0_itr,i][inds])
                    
        p    = P[dim0_itr,:,-1]
        p_err= P_err[dim0_itr,:,-1]
        p_cl = P_cl[dim0_itr,:,-1]
        p_cl_f = P_cl_flip[dim0_itr,:,-1]
        
        """
        # First table of absolute values
        table1 = np.vstack((p_in, p, p_cl, p_cl_f))[:, math.floor(len2/2)+1:]
        pres1 = np.full(math.floor(len2/2), fill_value=4)  # fixed precision of 4 decimals
        print("\n\n(Puolikas) data \nrivit: P;  P_qt,  P_cl;  P_cl_flip:")
        print("sarakkeet: P=0.2, 0.4, 0.6, 0.8, 1.0, mean, std_deviation")
        latex_format.print_to_latex_tabular(table1, column_precisions=pres1, significant_figures=False)
        
        # second table of relative values
        table2 = np.vstack(((p - p_cl) / p, 
                    (p_cl - p_cl_f) / p_cl,
                    1 - prob_fracs))
        means2 = np.mean(table2, axis=1)
        means2 = np.reshape(means2, (-1,1))
        devs2 = np.std(table2, axis=1)
        devs2 = np.reshape(devs2, (-1,1))
        
        table2 = np.concatenate((table2[:, math.floor(len2/2)+1:], means2, devs2), axis=1)
        pres2 = np.full(math.floor(len2/2) + 2, fill_value=3)
        print("\n\nSuhteelliset erot - taulukko:")
        print("sarakkeet: P=0.2, 0.4, 0.6, 0.8, 1.0, mean, std_deviation")
        print("rivit: (P-P_cl)/P;   (P_cl-P_cl_flip)/P_cl; (1-flip_osuus)")
        latex_format.print_to_latex_tabular(table2, column_precisions=pres2, significant_figures=True)
        """
        
        # third table that is actually in Bsc thesis
        table3 = np.vstack((p_in,
                            p,
                            p_err,
                            p_cl,
                            p_cl_f,
                            1 - prob_fracs))
        table3_P1 = table3[:,-1]
        pres3 = np.full(math.floor(len2/2) + 2, fill_value=3)
        print("\n\nVain arvolla P=+1 - taulukko:")
        print("sarakkeet: P=+1")
        print("rivit: p_in, p, p_err, p_cl, p_cl_f, 1 - prob_fracs")
        #latex_format.print_to_latex_tabular(table3_P1, column_precisions=table3_P1, significant_figures=True)
        print(table3_P1)
        
        

if __name__ == "__main__":
    main(sys.argv)
