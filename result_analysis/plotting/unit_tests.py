import unittest
import numpy as np
from . import plotting_1

class TestEnergyMatching(unittest.TestCase):

    def test_with_random(self):
        # big list
        for i in range(100):
            
            l_full = np.random.randint(20, 1000)
            l_sub = np.random.randint(1, int(l_full/10))
            
            arr_full = np.sort(np.random.random(l_full)) * 0.5
            
            # sub list indices
            real_inds = np.unique(np.random.randint(l_full, size=l_sub))
            
            # sub list
            arr_sub = np.sort(arr_full[real_inds])
            
            # calculated indices
            alg_inds = plotting_1.get_flip_indices_in_all_permutations(arr_full, arr_sub, err_label="XXX")
            
            all_the_same = all(alg_inds == real_inds)
            
            #print("real_inds", real_inds)
            #print("alg_inds", alg_inds)
            print(str(i)+" ", end="")
            
            self.assertTrue(all_the_same)
        print("")
        

if __name__ == '__main__':
    unittest.main()
