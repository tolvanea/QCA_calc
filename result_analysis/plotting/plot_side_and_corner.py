import os
import sys
import numpy as np
import matplotlib.pyplot as plt

    
from . import plot_dens
from . import plotting_1
import result_analysis.boltz_E.plotting as boltz_E_plotting
import result_analysis.read_calc_data.read_calc_data as read_calc_data


help_text = \
        """
        This script plots figures used in bachelor thesis. Specified for calulation sets: '5_wire' and '6_NOT'
        Implementations from 'plotting_1' are used.
        """


def main(argv=sys.argv):
    
    # |initial parameters of all calculations (aka. calculation set)
    # |           |pimc data of all calculations
    # |           |          |classical data of all calculations
    init_params, pimc_data, clas_data = get_all_data()
    
    # run the plotting
    side_and_corner(init_params, pimc_data, clas_data)
    
    plt.show()
    
    
def get_all_data():
    # check current directory is correct
    read_calc_data.initial_error_checking()
    
    # read initial parameters of calculation set, that are stored when the set is generated
    init_params = read_calc_data.read_initial_parameters()
    
    # read pimc-data and generate corresponding matlab-matrices in /results_data -directory.
    read_calc_data.octave_read_to_mat(init_params.dirname1, init_params.dirname2, init_params.len1, init_params.len2)
    
    # read the data from matrix that is generated above.
    pimc_data = read_calc_data.read_pimc_data_from_mat()
    
    # calculate or read classical data
    clas_data = read_calc_data.read_classical_system_data(os.getcwd(), init_params.dirlabels12, both_permutations=True)
    
    return init_params, pimc_data, clas_data
    

def side_and_corner(init_params, pimc_data, clas_data):
    """
    Sidewise and cornerwise coupling
    """
    
    set_name = os.path.basename(os.getcwd())
    # figure window 1 box
    fig1, ax1 = plt.subplots(1, 1, num='side_and_corner_coupling_' + set_name, figsize=(5,4))
    fig2, ax2 = plt.subplots(1, 1, num='side_and_corner_density_' + set_name, figsize=(5,4))
    fig3, ax3 = plt.subplots(1, 1, num='side_and_corner_histogram_' + set_name, figsize=(4,3.2))
    
    
    # figure 1 polarisation curves
    p_in = [plotting_1.q2p(q) for q in init_params.range2]
    P     = pimc_data.P
    P_err = pimc_data.P_err
    P_cl = clas_data.mean_pols  # last cell
    plot_fig1(ax1, p_in, P, P_err, init_params.dirlabels12, init_params.latex_syms12, init_params.range1, P_cl, set_name)
    
    # figure 2 density
    plot_fig2(ax2, init_params, clas_data, set_name)
    
    # figure 3 energylevel histogram
    annotations = (set_name == "1_perus")
    plotting_1.plot_histogram(ax3, clas_data, init_params.dirlabels12, idx1=-1, idx2=-1, 
                              annotations=annotations, title=False)
    
    # all 6 energy states, (this is for the Tikz plot validity in the Bsc. thesis)
    plot_fig4(init_params, clas_data, set_name)
    
    fig1.tight_layout()
    fig2.tight_layout()
    fig3.tight_layout(rect=(0, 0.07, 1, 1))

    fig1.savefig("results_data/" + set_name + "_side_and_corner_coupling" +'.pdf')
    fig2.savefig("results_data/" + set_name + "_side_and_corner_density" + '.pdf')
    fig3.savefig("results_data/" + set_name + "_side_and_corner_histogram" + '.pdf')
    
    #fig1.savefig("results_data/" + set_name + "_side_and_corner_coupling" +'.pgf')
    #fig2.savefig("results_data/" + set_name + "_side_and_corner_density" + '.pgf')
    #fig3.savefig("results_data/" + set_name + "_side_and_corner_histogram" + '.pgf')


def plot_fig1(ax1, p_in, P, P_err, dirlabels12, latex_syms12, range1, P_cl, set_name):
    """
    Plot first figure
    """
    
    for idx in range(len(dirlabels12[0])):
                        
        plotting_1.errorbar_xy(ax1, p_in, P[idx,:,-1], P_err[idx,:,-1],
                                      label="${} = {}\\,${} (PIMC)".format(
                                          latex_syms12[0][0], str(range1[idx]), latex_syms12[0][1]),
                                      linestyle="-", color="C"+str(idx)) # last cell
        ax1.plot(p_in, P_cl[idx, :, -1], \
                    label="${} = {}\\,${} (miehitysmalli)".format(
                                          latex_syms12[0][0], str(range1[idx]), latex_syms12[0][1]),
                    linestyle=":", color="C"+str(idx))
    
    plotting_1.plot_comparision_line(ax1, p_in, (P_cl[0, 0, -1] < P_cl[0, -1, -1]))
    
    ax1.set_xlabel("$P_0$", rotation=0, size=12)
    ax1.set_ylabel("$\\langle P_1 \\rangle$", size=12, rotation=0)
    ax1.set_xlim((-1,1))
    ax1.set_ylim((-1.001,1.001))
    #ax1.legend()
    #ax1.set_title("")
    
    plotting_1.fix_label_order_with_errorbars(ax1)

def plot_fig2(ax2, init_params, clas_data, set_name):
    """
    Plot second figure, density
    """
    
    plotting_1.plot_one_density_and_QDs(ax2, init_params, clas_data, idx1=-1, idx2=-1, title=False)
    
    if (set_name == "1_perus"):
        # labels visible
        ax2.text(-4.5, -2, "$P_0$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax2.text(0, -2, "$\\langle P_1 \\rangle$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        
    if (set_name == "2_not"):
        ax2.text(-4.5, -6.1, "$P_0$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        ax2.text(0, -2, "$\\langle P_1 \\rangle$", color="white", verticalalignment='center', horizontalalignment='center', fontsize=16)
        
        
def plot_fig4(init_params, clas_data, set_name):
    """
    Figure of all 6 energy states
    """
    
    # energy levels for one Tikz plot
    confs_128 = clas_data.get_single_data("confs_128", -1, -1)
    energies = clas_data.get_single_data("energies", -1, -1)
    energies_128 = (energies - energies[0])[0:min(128, len(energies))]
    fixed_particles = clas_data.get_single_data("fixed_particles", -1, -1)
    
    boltz_E_plotting.plot_permutations(energies_128, confs_128, fixed_particles, "Sidewise corner energystates in order", 
                                        plot_num = 6, num_subplots=6, save_name="results_data/visual_energy_states.pdf")
    
    
    

if __name__ == "__main__":
    main(sys.argv)
    
