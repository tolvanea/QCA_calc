import os
import sys
import numpy as np
import scipy.io as sio
from typing import List
import json
import subprocess
from pathlib import Path
import pickle

import result_analysis.boltz_E.main as boltz_E
import miscellaneous.general_tools as general_tools


"""
This module read pimc3-generated data and saves them in matlab matrix, that can be accesed later.

It also is for reading that matlab matrix

And also for stroring and reading classical caluclation data
"""


def main(argv):
    raise Exception("This is a module!, It is called from plotting functions")
    

def shitty_octave_script_format(dirname1, dirname2, range1, range2, dirlabels1, dirlabels2):
    """
    When I first made my octave scipts, they were a hack. Now when I use them, 
    they are still the same hack.
    """
    num_decimals1 = 999
    num_decimals2 = 999
    
    # if, for some reason, number of decimals in folders is not constant, choose it to be minimum.
    for filename1 in os.listdir():
        if filename1 in dirlabels1:
            num_decimals1 = min(num_decimals1, len(filename1) - len(dirname1))
            
    for filename2 in os.listdir(path=dirlabels1[0]):
        if filename2 in dirlabels2:
            num_decimals2 = min(num_decimals2, len(filename2) - len(dirname2))
    
    if (num_decimals1 == 999) or (num_decimals2 == 999):
        raise Exception("Could not find generated directory tree")
    
    return num_decimals1, num_decimals2
    
    
def initial_error_checking():
    parent_dir_name = os.path.basename(os.getcwd())
    list_dir = os.listdir()
    
    if not ("sample" in list_dir):
        raise Exception("You must run this in same directory with 'sample'")
    if parent_dir_name == "sample":
        print("'sample' is very misleading name for a parent folder.")


class InitParams():
    """
    Data structure for initial parameters in calculation set
    """
    def __init__(self, dirname1, dirname2, range1, range2, dirlabels1, dirlabels2, latex_sym1, latex_sym2):
        self.dirname1 = dirname1      # directory basename without variating number
        self.dirname2 = dirname2
        
        self.range1 = range1        # list iterating values
        self.range2 = range2
        self.range12 = [self.range1, self.range2]
        
        self.dirlabels1 = dirlabels1    # directory basename + number from range of iterating value
        self.dirlabels2 = dirlabels2
        self.dirlabels12 = [self.dirlabels1, self.dirlabels2]
        
        self.latex_variable_sym1 = latex_sym1[0]
        self.latex_variable_sym2 = latex_sym2[0]
        self.latex_unit_sym1 = latex_sym1[1]
        self.latex_unit_sym2 = latex_sym2[1]
        self.latex_syms12 = [latex_sym1, latex_sym2]
        
        len1, len2 = shitty_octave_script_format(dirname1, dirname2, range1, range2, dirlabels1, dirlabels2)
        self.len1 = len1  # for octave script hack, tells the number of characers in the directory name number-part
        self.len2 = len2  


def read_initial_parameters():
    """
    Read file that was created when calulation was initialized. Now includes latex labels
    """
    
    with open('sample/configuration_info.json', 'r') as file:
        
        # Tuple[str, str, List[float], List[float], List[str]]
        dump = json.load(file)
        
        if len(dump) == 8:
            init_params = InitParams(*dump)
            # the previous line does following:
            # dirname1, dirname2, range1, range2, dirlabels1, dirlabels2, latex_sym1, latex_sym2 = dump
            # init_params = InitParams(dirname1, dirname2, range1, range2, dirlabels1, dirlabels2, latex_sym1, latex_sym2)
        else:
            raise Exception("Malformed initial parameters file")

    return init_params

def octave_read_to_mat(dirname1, dirname2, len1, len2):
    """
    This is a mess. 
    Generate matlab matrix files with external octave script.
    """
    
    # Run octave in separate process
    
    if not os.path.exists("results_data"):
        os.makedirs("results_data")
    else:
        if os.path.isfile("results_data/pol_fid.mat") and os.path.isfile("results_data/energies.mat"):
            print("Found already created mat.file")
            return
            
    # add this path by hand to \.octaverc
    path_to_octave_script = os.path.dirname(__file__) + "/read_calc_data"
    octaverc_line = 'addpath("' + path_to_octave_script + '")'
    
    homedir = str(Path.home())
    with open(homedir+"/.octaverc", "r") as file:
        lines = file.readlines()
    
    found = False
    for line in lines:
        indx = line.find('#') # theres comment at the end
        if indx >= 0:
            bare_line = line[0:indx].strip()
            found = True
    
    if not found:
        inp = input("Add following line to ~\.octaverc ?\n" + octaverc_line+"[y/n] (answer y) \n")
        if (inp == "y") or (inp == "Y") or (inp == ""):
            with open(homedir+"/.octaverc", "a") as file:
                file.write("\n" + octaverc_line + "# Added by QCA-plotting scripts\n")
        else:
            raise Exception("This program is not gonna work without that modification")
    
    command = 'save_results_to_mat({}, {}, {}, {})'.format('"'+dirname1+'"', len1, '"'+dirname2+'"', len2) 
    process = subprocess.Popen(["octave", "--silent", "--no-gui", "--eval", command ], 
                               cwd=os.getcwd(), stdout = subprocess.PIPE, stderr=subprocess.PIPE)
    # process.wait() # not suitable
    stdout, stderr = process.communicate(timeout=60)
    return_code = process.poll()
    if len(stdout) < 5:
        print("octave STDOUT ", stdout)
    else:
        print("octave STDOUT ", stdout.decode("utf-8") )
    if len(stderr) < 5:
        print("octave STDERR ", stderr)
    else:
        print("octave STDERR ", stderr.decode("utf-8") )
        

class PimcData():
    """
    load all info from data .mat structures
    """
    def __init__(self, pol_fid, Et_Ep_Ek):
        self.P      = np.array(pol_fid["P"]       )
        self.P_err  = np.array(pol_fid["P_err"]   )
        self.F      = np.array(pol_fid["F"]       )
        self.F_err  = np.array(pol_fid["F_err"]   )
        self.Et     = np.array(Et_Ep_Ek["Et"]     )
        self.Et_err = np.array(Et_Ep_Ek["Et_err"] )
        self.Ep     = np.array(Et_Ep_Ek["Ep"]     )
        self.Ep_err = np.array(Et_Ep_Ek["Ep_err"] )
        self.Ek     = np.array(Et_Ep_Ek["Ek"]     )
        self.Ek_err = np.array(Et_Ep_Ek["Ek_err"] )
        
        if len(self.P.shape) < 3: # damn octave bug
            self.P      = np.expand_dims(self.P    ,2) 
            self.P_err  = np.expand_dims(self.P_err,2)
            self.F      = np.expand_dims(self.F    ,2)
            self.F_err  = np.expand_dims(self.F_err,2)
            self.Et     = np.expand_dims(self.Et    ,2)
            self.Et_err = np.expand_dims(self.Et_err,2)
            self.Ep     = np.expand_dims(self.Ep    ,2)
            self.Ep_err = np.expand_dims(self.Ep_err,2)
            self.Ek     = np.expand_dims(self.Ek    ,2)
            self.Ek_err = np.expand_dims(self.Ek_err,2)


def read_pimc_data_from_mat():
    # Read generated .mat files
    
    if not os.path.exists("results_data"):
        raise Exception("Generate data with octave_read_to_mat() before calling this!")
    
    contents_pol_fid = sio.loadmat('results_data/pol_fid.mat')
    contents_Et_Ep_Ek = sio.loadmat('results_data/energies.mat')
    
    pimc_data = PimcData(contents_pol_fid, contents_Et_Ep_Ek)
    
    return pimc_data


class ClassicalData():
    """
    Classical data in calculation set
    
    cl_data is matrix with dimensions of calculation set: (var_1  x  var_2)
    every cell is a dictionary of classical data.
    
    "thermal_pols"  polarisation means of every cell
    "probs"         probability of every state
    "energies"      energy of every state. Oh and btw, the are not normed to E_0, but to infinity distanges
    "pols"          in every state, discrete polarisation of every cell
    "temperature"   temperature-constant
    "dielectric"    dielectric-constant
    
    (optional)
    These following values are present only if they are added in 'read_classical_system_data'.
    If these are defined, then values above, i.e. "energies" contains all cell permutations, 
    and these contains are only flip permutations.
    
    "thermal_pols_flip" 
    "probs_flip"        
    "energies_flip"     
    "pols_flip"         
    
    "energies_flip" energy states of only flip permutations, 
    """
    def __init__(self, cl_data):
        self.cl_data = cl_data
        self.mean_pols = self.get_mean_pols()
        self.mean_Ep = self.calc_mean_energy()
        self.temperature = self.get_all_data("temperature")
    
    def get_mean_pols(self, dataname="thermal_pols"):
        l1 = len(self.cl_data)
        l2 = len(self.cl_data[0])
        num_cells = len(self.cl_data[0][0][dataname])
        
        pol = np.empty((l1, l2, num_cells))
        for i in range(l1):
            for j in range(l2):
                pol[i,j,:] = self.cl_data[i][j][dataname]
        
        return pol
    
    def calc_mean_energy(self, dataname="energies"):
        l1 = len(self.cl_data)
        l2 = len(self.cl_data[0])
        
        Ep = np.empty((l1, l2))
        for i in range(l1):
            for j in range(l2):
                energies = self.cl_data[i][j][dataname] # numpy arrays
                probs = self.cl_data[i][j]["probs"]
                E_mean = np.sum(energies * probs)
                Ep[i,j] = E_mean
        return Ep
    
    def get_single_data(self, dataname, idx1, idx2):
        return self.cl_data[idx1][idx2][dataname]
    
    def get_all_data(self, dataname):
        l1 = len(self.cl_data)
        l2 = len(self.cl_data[0])
        #l3 = len(self.cl_data[0][0][dataname])
        
        # if data is vector-type (and not scalar like temperature), convert it to numpy array
        if hasattr(self.cl_data[0][0], "__len__"):
            data = np.empty((l1, l2), dtype=type(np.array(self.cl_data[0][0])))
            for i in range(l1):
                for j in range(l2):
                    data[i,j] = np.array(self.cl_data[i][j][dataname])
        else:
            data = np.empty((l1, l2), dtype=type(self.cl_data[0][0]))
            for i in range(l1):
                for j in range(l2):
                    data[i,j] = self.cl_data[i][j][dataname]
                
        return data


def read_classical_system_data(path, dirlabels12, both_permutations=False):
    """
        Args:
            both_permutations   if false:   depending of system size the best option is selected for 
                                            'energies': flip_only or permute_cell
                                if true:    'energies' is permute_cell and 
                                            'energies_flip' is flip_only
    """
    
    if os.path.isfile("results_data/classical_data.pkl"):
        with open('results_data/classical_data.pkl', 'rb') as file:
            cl_data = pickle.load(file)
        print("Using already created classical data")

    else:
        # caluclate energies by 'flip_only' or 'permute_cell', depending on system size
        if not both_permutations:
            func = lambda path: boltz_E.clas_stat_polarizations(path, check_states="choose-best", not_verbose=True)
            cl_data = map_for_all_calculations(func, path, dirlabels12)
        # do both 'permute_cell' and 'flip_only'
        else:
            func_all = lambda path: boltz_E.clas_stat_polarizations(path, check_states="permute_cell", not_verbose=False)
            func_flip = lambda path: boltz_E.clas_stat_polarizations(path, check_states="flip_only", not_verbose=True)
            
            cl_data = map_for_all_calculations(func_all, path, dirlabels12)
            cl_data_flip = map_for_all_calculations(func_flip, path, dirlabels12)
            
            # add second energies_flip to cl_data
            for i in range(len(cl_data)):
                for j in range(len(cl_data[0])):
                    cl_data[i][j].update({"thermal_pols_flip"   : cl_data_flip[i][j]["thermal_pols"]})
                    cl_data[i][j].update({"probs_flip"          : cl_data_flip[i][j]["probs"]})
                    cl_data[i][j].update({"energies_flip"       : cl_data_flip[i][j]["energies"]})
                    cl_data[i][j].update({"pols_flip"           : cl_data_flip[i][j]["pols"]})
                    cl_data[i][j].update({"confs_128_flip"      : cl_data_flip[i][j]["confs_128"]})
                    cl_data[i][j].update({"fixed_particles_flip": cl_data_flip[i][j]["fixed_particles"]})

        if general_tools.get_size(cl_data) < 1000*10**6: # 1000MB max file size
            with open('results_data/classical_data.pkl', 'wb') as file:
                pickle.dump(cl_data, file)
    
    clas_data = ClassicalData(np.array(cl_data))

    return clas_data


def map_for_all_calculations(func, path, dirlabels12): # -> List[List[func_return]]:
    """
    For all calculations run func, (used in classical energy level calulations)
    Args:
        func            must be a function that accepts only one parameter which is path
                        # example:
                        # func = lambda path: plot_dens.plot_remote(x,y,z,path=path)
        path            base for calc dir paths to append
        dirlabels12     directory labels of first and second layer
    """
    
    l1 = len(dirlabels12[0])
    l2 = len(dirlabels12[1])
    cl_data = [[None for i in range(l2)] for i in range(l1)]
    
    print("Classical energy level combinations: (#/" + str(l1*l2) + "): ", end="")
    
    for i, dirlabels1 in enumerate(dirlabels12[0]):
        for j, dirlabels2 in enumerate(dirlabels12[1]):
            path_append = path + '/' + dirlabels1 + '/' + dirlabels2
            #os.chdir(path)
            cl_data[i][j] = func(path_append)
            print("#",end="")
            sys.stdout.flush()
            #os.chdir("../..")
    
    print("")
    
    return cl_data


if __name__ == "__main__":
    main(sys.argv)
