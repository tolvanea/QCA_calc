

# This file saves data (polarisation, fidelity, etc..) to matrix/container 
# which is read with python.

# usage

function save_results_to_mat(sym1, len1, sym2, len2)    
    [dir_names1, dir_names12, var_val_cellarr1, var_val_cellarr2, var_vals1, var_vals2] = ...
                    read_all_directory_names_twice(pwd, sym1, len1, sym2, len2);
    
    [P, P_err, F, F_err, Et, Et_err, Ep, Ep_err, Ek, Ek_err] = ...
                    read_observables(pwd, dir_names1, dir_names12, var_val_cellarr1, var_val_cellarr2, var_vals1, var_vals2);
    
    save("-mat7-binary", "results_data/pol_fid.mat", "P", "P_err", "F", "F_err");
    save("-mat7-binary", "results_data/energies.mat", "Et", "Et_err", "Ep", "Ep_err", "Ek", "Ek_err");
    
    #pwd
    #disp "P"
    #P
end


# 'observable_reading' contains all other functions



% Brief: return observables for set of PIMC-calculations
% Params
%       dir_names1  directory names of first depth
%       dir_names12 2d-array of directory names, where first dim corresponds different dirs of first depth
%       C1          variating value cell arr of first dir-depth
%       C2          variating value cell arr of second dir-depth, notice all vals must be equal in each dir
%       var_vals1   variating value vector of second dir-depth
%       var_vals2   variating value vector of second dir-depth, notice all vals must be equal in each dir
%
% Returns                                        (matrix(row_indx,column_indx))
%       P         Polarisation                   format: (matrix(index1,index2))
%       P_err                 error                       where each item is vector of cells
%       F         Fidelity                       format: (matrix(index2,index2))
%       F_err             error                           where each item is vector of cells
%       Et        Total energy
%       Et_err            error
%       Ep        Potential E
%       Ep_err            error
%       Ek        Kinetic E
%       Ek_err            error
function [P, P_err, F, F_err, Et, Et_err, Ep, Ep_err, Ek, Ek_err] = ...
                read_observables(path, dir_names1, dir_names12, C1, C2, var_vals1, var_vals2)
    
    for i1 = 1:length(dir_names1)
        dirName_1 = dir_names1{i1};
        for i2 = 1:size(dir_names12,2)
            dirName_2 = dir_names12{i1,i2};
            
            [p, p_err, f, f_err] = read_pol_and_fid_means(...
                        strcat(path,'/',dirName_1,'/',dirName_2));
            [et, et_err, ep, ep_err, ek, ek_err] = read_energy_means(...
                        strcat(path,'/',dirName_1,'/',dirName_2));
            
            # first time this is run
            if i1 == 1 && i2 == 1
                num_cells = length(p);
            
                P      = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                P_err  = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                F      = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                F_err  = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Et     = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Et_err = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Ep     = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Ep_err = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Ek     = zeros(length(dir_names1), size(dir_names12,2), num_cells);
                Ek_err = zeros(length(dir_names1), size(dir_names12,2), num_cells);
            end
            
            %disp P(i1,i2,:)
            %P(i1,i2,:)
            %disp p
            %p
            
            P(i1,i2,:)     = p;
            P_err(i1,i2,:) = p_err;
            F(i1,i2,:)     = f;
            F_err(i1,i2,:) = f_err;
            Et(i1,i2,:)     = et;
            Et_err(i1,i2,:) = et_err;
            Ep(i1,i2,:)     = ep;
            Ep_err(i1,i2,:) = ep_err;
            Ek(i1,i2,:)     = ek;
            Ek_err(i1,i2,:) = ek_err;
        end
    end
end



% This function reads one of calculations' directory and returns mean polarizations
% and fidelities (and errors) in vector for each cell. The first 10 values are drop off.
% This function is called from main part of script
% Arguments
%       path    path to calculation folder
% Returns:
%       p       mean polarization           (row vector of all cells)
%       p_err   error in mean polarization  (row vector of all cells)
%       f       fidelity                    (row vector of all cells)
%       f_err   error in mean fidelity      (row vector of all cells)
function [p, p_err, f, f_err] = read_pol_and_fid_means(path)

    if (nargin == 0) % optinal path-parameter
        path = strcat(pwd,'/');
    end
    if (path(end) != '/')
        path = strcat(path,'/');
    end

    p = [];
    p_err = [];
    f = [];
    f_err = [];


    % All filenames in obs001/ directory that begins with 'Polarization'
    pol_names = fileNames(strcat(path,'obs001/'), 'Polarization.');
    fid_names = fileNames(strcat(path,'obs001/'), 'Fidelity.');

    #{
    var_vals1 = cellfun(@str2double,C1'); # so C1 is cell_arr<string> and var_vals is vector<int>
    var_vals2 = cellfun(@str2double,C2');
    [var_vals1, sorted_indices1] = sort(var_vals1);
    [var_vals2, sorted_indices2] = sort(var_vals2);
    C1 = C1(sorted_indices1);
    C2 = C2(sorted_indices2);

    str_indx_2 = findstr(dirName_2, variating_symbol_2);
    #}
    
    #{
    # order fid and pol_names
    index_list = [];
    for cell_indx = 1:size(pol_names, 2)
        name = pol_names{1,cell_indx};
        dot_idx = findstr(name, '.');
        count_number_str = name(dot_idx(1)+1:dot_idx(2)-1);
        index_list(cell_indx) = str2double(count_number_str);
    end
    [~, index_list] = sort(index_list);
    pol_names = pol_names(index_list);
    fid_names = fid_names(index_list);
    #}

    % iterate, for example, 4 different cells' polarizations
    for cell_indx = 1:size(pol_names, 2)
        polarization_file_name = pol_names{1,cell_indx};
        fidelity_file_name = fid_names{1,cell_indx};

        % loads vectors from file
        pol_vec = load(strcat(path,'/obs001/', polarization_file_name));
        fid_vec = load(strcat(path,'/obs001/', fidelity_file_name));

        [p_mean,p_mean_err] = sem2(pol_vec(1:end));
        [f_mean,f_mean_err] = sem2(fid_vec(1:end));

        #disp(p_mean)

        p(1,cell_indx) = p_mean;
        p_err(1,cell_indx) = p_mean_err;

        f(1,cell_indx) = f_mean;
        f_err(1,cell_indx) = f_mean_err;
    end

end



% Returns cell array of filenames that contains given string in the beginning of file name
% This function is called from read_pol_and_fid_means()
% Arguments
%       path    path to directory, in this case obs001/
%       str     string that is in the beginning of that file name
% Returns
%       names   cell array of found names
function names = fileNames(path, str)
    %https://se.mathworks.com/matlabcentral/newsreader/view_thread/162472
    files = dir(path);
    fileIndex = find(~[files.isdir]);
    names = {}; % strings and arrays isn't a good combination, (=int matrix)
    for i = 1:length(fileIndex)
        fileName = files(fileIndex(i)).name;
        %fprintf('Tiedoston nimi %s', fileName)
        %disp( findstr(fileName, str))
        if (findstr(fileName, str) == 1)
            names{end+1} = fileName;
        end
    end
end


function [Et,Et_err, Ep,Ep_err, Ek,Ek_err] = read_energy_means(path)
    # error checkin begins
    if (path(end) != '/')
        path = strcat(path,'/');
    end
    [parent_dir,current_dir,~]=fileparts(path(1:end-1));
    if (strcmp(current_dir, "obs001"))
        path = strcat(parent_dir,'/');
    end
    # error checkin ends


    Et = load(strcat(path,'obs001/Et.dat'));
    Ep = load(strcat(path,'obs001/Ep.dat'));
    Ek = load(strcat(path,'obs001/Ek.dat'));

    [Et,Et_err] = sem2(Et(3:end));
    [Ep,Ep_err] = sem2(Ep(3:end));
    [Ek,Ek_err] = sem2(Ek(3:end));
end


                
                
% Retrurns
%       dir_names1  directory names of first depth
%       dir_names12 2d-array of directory names, where first dim corresponds different dirs of first depth
%       C1          variating value cell arr of first dir-depth
%       C2          variating value cell arr of second dir-depth, notice all vals must be equal in each dir
%       var_vals1   variating value vector of second dir-depth
%       var_vals2   variating value vector of second dir-depth, notice all vals must be equal in each dir
function [dir_names1, dir_names12, C1, C2, var_vals1, var_vals2] = read_all_directory_names_twice(path, variating_symbol_1, num_value_chars_1, variating_symbol_2, num_value_chars_2)

    if (path(end) != '/')
        path = strcat(path,'/');
    end

    C1 = {};
    #C2 = {}; # I really do not understand octaves variable scope. Is it bug? No it was not

    dir_names1={}; # names that will used as valid directories (first round)
    dir_names12={}; # (second round)

    dirs_1 = dir(path);
    dirIndex_1 = find([dirs_1.isdir]);
    # iterate through all directories of calulcations
    for i1 = 1:length(dirIndex_1)
        dirName_1 = dirs_1(dirIndex_1(i1)).name;
        #disp("1. - I'm here!")
        #disp(dirName_1)

        if (dirName_1(1) != '.')  % drops out ./ and ../
            str_indx_1 = findstr(dirName_1, variating_symbol_1);
            #disp("2. - I'm here! str_indx_1 variating_symbol_1")
            #disp(str_indx_1)
            #disp(variating_symbol_1)
            if (length(str_indx_1) == 1)    % includes filenames of only one R
                #disp("3. - I'm here!")
                % Now parse values of r from names of directories
                % Minus sign requires some processing
                sym_len_1 = length(variating_symbol_1);
                if (str_indx_1+sym_len_1+num_value_chars_1-1 > length(dirName_1))
                    dirName_1
                    str_indx_1
                    num_value_chars_1
                    disp('Variable len is too long!')
                end

                if (dirName_1(str_indx_1+sym_len_1) != '-')
                    variating_value_1 = dirName_1(str_indx_1+sym_len_1:str_indx_1+sym_len_1+num_value_chars_1-1);
                else
                    variating_value_1 = dirName_1(str_indx_1+sym_len_1:str_indx_1+num_value_chars_1+sym_len_1);
                end

                dir_names1{end+1,1} = dirName_1;
                C1{end+1,1} = variating_value_1;
                index1 = size(C1,1);

                % now the same basicly again, assuming coherent naming in grid
                C2 = {};



                dirs_2 = dir(strcat(path,'/',dirName_1));
                dirIndex_2 = find([dirs_2.isdir]);


                # iterate through all directories of calulcations
                for i2 = 1:length(dirIndex_2)
                    dirName_2 = dirs_2(dirIndex_2(i2)).name;

                    if (dirName_2(1) != '.')  % drops out ./ and ../
                        str_indx_2 = findstr(dirName_2, variating_symbol_2);

                        if (length(str_indx_2) == 1)    % includes filenames of only one R
                            % Now parse values of r from names of directories
                            % Minus sign requires some processing
                            sym_len_2 = length(variating_symbol_2);
                            if (str_indx_2+sym_len_2+num_value_chars_2-1 > length(dirName_2))
                                error('Variable len is too long! Are you not using string e.g. sym="1"?')
                            end

                            if (dirName_2(str_indx_2+sym_len_2) != '-')
                                variating_value_2 = dirName_2(str_indx_2+sym_len_2:str_indx_2+sym_len_2+num_value_chars_2-1);
                            else
                                variating_value_2 = dirName_2(str_indx_2+sym_len_2:str_indx_2+num_value_chars_2+sym_len_2);
                            end

                            C2{end+1,1} = variating_value_2;
                            index2 = size(C2,1);
                            dir_names12{index1,index2} = dirName_2;

                        end
                    end
                end

            end
        end
    end
    if (length(C1) == 0)
        disp(strcat("Path you are working in:",path))
        error(strcat("Nothing found with variating_symbol_1:", variating_symbol_1))
    end
    if (length(C2) == 0)
        error(strcat("Nothing found with variating_symbol_2:", variating_symbol_2))
    end
    # Great, now we have all file names in <1d-array>:dir_names1 and <2d-array>:dir_names12,
    # and we have all iterable values in C1 and C2.

    # sort them
    var_vals1 = cellfun(@str2double,C1'); # so C1 is cell_arr<string> and var_vals is vector<int>
    var_vals2 = cellfun(@str2double,C2');
    [var_vals1, sorted_indices1] = sort(var_vals1);
    [var_vals2, sorted_indices2] = sort(var_vals2);
    C1 = C1(sorted_indices1);
    C2 = C2(sorted_indices2);
    dir_names1 = dir_names1(sorted_indices1);
    dir_names12 = dir_names12(sorted_indices1,:);
    dir_names12 = dir_names12(:,sorted_indices2);

end
