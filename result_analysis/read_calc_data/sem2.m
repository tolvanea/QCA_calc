% This matlab script-file has been provided by Juha Tiihonen
% (Therefore I do not take any credit, or can say that it is 
% under the same licence (MIT) as rest of project)

% Funktio keskiarvon keskivirheen laskemiseen

function [E,err]=sem2(Eb,alku,loppu)

if nargin==1
  alku=1;
  loppu=length(Eb);
elseif nargin==2
  loppu=length(Eb);
end

i=alku:loppu;
E=mean(Eb(i,:));
if (mean(Eb) == 0)
  err=0;
else
  err=2*std(Eb(i,:)).*sqrt(corrtime(Eb(i,:))./length(Eb(i,:)));
end


function kappa = corrtime(dp)

avg = mean(dp);
N = length(dp);
variance = var(dp);

kappa = zeros(1,size(dp,2));
for k = 1:size(dp,2)
  for i = 1:N
    coef = C(dp(:,k),i,N,avg(k),variance(k));
    if (coef < 0)
      break;
    else
      kappa(k) = kappa(k) + coef;
    end
  end
end
kappa = 1 + 2*kappa;


function coeff = C(dp,i,N,avg,variance)

coeff = 0;
for j = 1:N-i
  coeff = coeff + (dp(j)-avg)*(dp(j+i)-avg);
end
coeff = coeff/(N-i)/variance;
